#ifndef ABSTRACTVALUEACCESSOR_H
#define ABSTRACTVALUEACCESSOR_H

#include "AbstractAccessor.h"
#include "AbstractComparable.h"
#include <memory>

namespace persistent_containers
{
	
	/*template<typename T>
		requires(std::is_trivially_copyable<T>::value)*/
		
	/** interface for accessing memory buffers
	*/
	class AbstractComparableAccessor : public AbstractAccessor, public AbstractComparable
	{
	public:

		/** returns number of bytes to read from node
		@return number of bytes to read from node
		*/
		virtual constexpr std::size_t getBytesToRead()
		{
			return 0;
		}

		/** fetch element from position
		*/
		virtual BufferView getElementFrom(
			char* pos ///< storage-position in node
			)
		{
			return BufferView();
		}

		/** put element to position
		*/
		virtual constexpr void putElementTo(
			char* pos, ///< position in memory; has to be in an node
			BufferView& elem ///< memory slice holding the entry
			)
		{}
		
		/** compare two MemoryBuffers
		@return -1 if a > b; 0 if a==b and 1 if a < b
		*/
		virtual char compare(
			BufferView a,	///< element a to compare
			BufferView b  ///< element b to compare
			)
		{
			return 0;
		}

		/*
		virtual T get(BufferView mb)
		{
			return 0;
		}

		virtual void set(BufferView mb, T val)
		{
		}
		*/

	};
}

#endif
