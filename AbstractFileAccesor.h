#ifndef ABSTRACTFILEACCESSOR_H
#define ABSTRACTFILEACCESSOR_H

#include <memory>
#include <type_traits>
#include "AbstractBuffer.h"

namespace persistent_storage
{

	template<typename positionT>
	class AbstractFileAccessor : public std::enable_shared_from_this<AbstractFileAccessor<positionT>>
	{
	public:
		virtual std::shared_ptr<persistent_storage::AbstractBuffer> getFromPos(positionT pos, std::size_t size);

		virtual void putToPos(std::shared_ptr<persistent_storage::AbstractBuffer>& buf, positionT pos);

		virtual positionT reserveSpace(std::size_t size);

		virtual void removeSpaceReservation(positionT pos, std::size_t size);
	};
}

namespace persistent_storage
{
	template<typename positionT>
	std::shared_ptr<persistent_storage::AbstractBuffer> AbstractFileAccessor<positionT>::getFromPos(positionT pos, std::size_t size)
	{
		throw std::logic_error("calling abstract method");
	}

	template<typename positionT>
	void AbstractFileAccessor<positionT>::putToPos(std::shared_ptr<persistent_storage::AbstractBuffer>& buf, positionT pos)
	{
		throw std::logic_error("calling abstract method");
	}

	template<typename positionT>
	positionT AbstractFileAccessor<positionT>::reserveSpace(std::size_t size)
	{
		throw std::logic_error("calling abstract method");
	}

	template<typename positionT>
	inline void AbstractFileAccessor<positionT>::removeSpaceReservation(positionT pos, std::size_t size)
	{
		throw std::logic_error("calling abstract method");
	}
}
#endif
