#ifndef FREESPACEMANAGINGFILEACCESSOR_H
#define FREESPACEMANAGINGFILEACCESSOR_H

#include "AbstractFileAccesor.h"
#include "storedInsideComparableAccessor.h"
#include "tree2.h"
#include <memory>

namespace persistent_storage
{

	/** Manages slices of foremerly used file-areas.
	* 
	* When a block is freed, its position and dimensions are written into two sets. One set contains
	* the position and the size and the other set contains the size and a pointer to slices of file-
	* areas.
	*/
	template<typename positionT>
	class FreespaceManagingFileAccessor :  public persistent_storage::AbstractFileAccessor<positionT>
	{

	public:
		/** Constructor
		*/
		FreespaceManagingFileAccessor(
			std::fstream &f ///< Reference to a file-stream-object
		) :
			file(f),
			cache{}
		{
		}

		/** Destructor
		*/
		~FreespaceManagingFileAccessor()
		{
			//file.close(); // caller has to close
		}

		/* this-shared-ptr is not available in constructor */
		/** Initializes the data-structures
		* 
		* shared_for_this() fails in constructor, so we need this to link the two sets file-management.
		* 
		*/
		void initialize(positionT p)
		{
			free_spaces_by_pos = std::make_shared<persistent_containers::Tree<positionT>>(this->shared_from_this(), p, std::make_shared<persistent_containers::storedInsideComparableAccessor<positionT>>(), std::make_shared<persistent_containers::storedInsideAccessor<positionT>>());
			free_spaces_by_size = std::make_shared<persistent_containers::Tree<positionT>>(this->shared_from_this(), p+sizeof(positionT), std::make_shared<persistent_containers::storedInsideComparableAccessor<positionT>>(), std::make_shared<persistent_containers::storedInsideAccessor<positionT>>());
		
			free_spaces_by_pos->set_root_pos(0); // mark empty
			free_spaces_by_size->set_root_pos(0);// mark empty

			std::size_t tmp = file.tellp();
			file.seekp(0, std::ios_base::end);
			physical_end = static_cast<positionT>(file.tellp());
			file.seekp(tmp, std::ios_base::beg);


		}

		/** Flushes the file
		* 
		* consider marking deprecated
		*/
		void flush()
		{
			file.flush();
		}

		/** Fetches a Buffer from the file
		* 
		* If the slice is in the cached buffers, it is returned. Otherwise it's fetched from
		* the file and put into the caches.
		*/
		std::shared_ptr<persistent_storage::AbstractBuffer> getFromPos(
			positionT pos,		///< Position th fetch
			std::size_t size	///< size of the slice to fetch
		) override
		{
			std::shared_ptr<persistent_storage::AbstractBuffer> ret;
			try
			{
				ret = cache.at(pos);
				if (ret->get_size() != size)
				{
					throw std::runtime_error("ret->getSize() != size");
				}
			}
			catch (std::out_of_range)
			{
				std::shared_ptr<persistent_storage::AbstractBuffer> buf = std::make_shared<persistent_storage::Buffer>(size);
				file.seekg(pos);
				file.read(buf->get_ptr(), size);
				if (file.bad())
					throw std::runtime_error("file bad()");
				cache[pos] = buf;
				ret = buf;
			}
			return ret;
		}

		/** Put buffer in cache for later flushing
		* 
		* consider adding debug-defines for checking overwriting
		*/
		void putToPos(std::shared_ptr<persistent_storage::AbstractBuffer>& buf, positionT pos) override
		{
			cache[pos] = buf;
		}

		/** Puts caches to file.
		* 
		*/
		void flushCache()
		{
			std::map<positionT, std::shared_ptr<persistent_storage::AbstractBuffer>> remaining;
			for (auto& i : cache)
			{
				positionT pos = i.first;
				std::shared_ptr<persistent_storage::AbstractBuffer> buf = i.second;
				if (buf->is_dirty())
				{
					std::size_t to_write = buf->get_size();
					file.seekp(pos);
					file.write(buf->get_ptr(), to_write);
					if (file.bad())
					{
						throw std::runtime_error("file bad() in flushCache()");
					}
				}
				if (i.second.use_count() > 2) // keep in cache if still in use
				{
					remaining[i.first] = i.second;
				}
			}
			cache.clear();
			cache.swap(remaining);
		}

		/** reserve space in file.
		* 
		* Looks up free position or extends file.
		* 
		* @return position of size size
		*/
		positionT reserveSpace(
			std::size_t size ///< size of the needed area
		) override
		{
			positionT pos = physical_end;

			// determine pos
			persistent_containers::BufferView mb_size((char*) &size, sizeof(size));
			try
			{
				auto size_iter(free_spaces_by_size->get_closest(mb_size));

				// make sure, size_iter points to a tree containing regions larger than size
				if (dynamic_cast<persistent_containers::storedInsideAccessor<positionT>*>(size_iter.get_node_ptr()->getOrigin()->iA.get())->get((*size_iter).first) < size)
				{
					size_iter++;
				}

				// 
				if(size_iter != free_spaces_by_size->end() && dynamic_cast<persistent_containers::storedInsideAccessor<positionT>*>(size_iter.get_node_ptr()->getOrigin()->iA.get())->get((*size_iter).first) >= size)
				{
					positionT pos_pos = dynamic_cast<persistent_containers::storedInsideAccessor<positionT>*>(size_iter.get_node_ptr()->getOrigin()->vA.get())->get((*size_iter).second);
					std::shared_ptr<persistent_containers::storedInsideComparableAccessor<positionT>> pos_pos_iA = std::make_shared<persistent_containers::storedInsideComparableAccessor<positionT>>();
					std::shared_ptr<persistent_containers::storedInsideAccessor<positionT>> pos_pos_vA = std::make_shared<persistent_containers::storedInsideAccessor<positionT>>();
					persistent_containers::Tree<positionT> pos_pos_tree(this->shared_from_this(), pos_pos, pos_pos_iA, pos_pos_vA);
					try
					{
						auto first = pos_pos_tree.begin();

						// do not use cached elements (would result in "ret_size != size"-errors)
						while (first != pos_pos_tree.end() && cache.count(dynamic_cast<persistent_containers::storedInsideAccessor<positionT>*>(first.get_node_ptr()->getOrigin()->iA.get())->get((*first).first)) != 0)
						{
							first++;
						}

						// if iterator is valid, fetch next tree, containing positions mapping to elements in the free_spaces_by_pos_tree
						if (first != pos_pos_tree.end())
						{
							pos = dynamic_cast<persistent_containers::storedInsideAccessor<positionT>*>(first.get_node_ptr()->getOrigin()->vA.get())->get((*first).second);

							// if block is larger than needed, split area in two parts
							if (dynamic_cast<persistent_containers::storedInsideAccessor<positionT>*>(size_iter.get_node_ptr()->getOrigin()->iA.get())->get((*size_iter).first) > size)
							{
								bool create_new = false;
								positionT newPos = pos + size;
								positionT newSize = dynamic_cast<persistent_containers::storedInsideAccessor<positionT>*>(size_iter.get_node_ptr()->getOrigin()->vA.get())->get((*size_iter).second) - size;

								positionT subtree_pos = -1;

								persistent_containers::BufferView mb_newSize((char*) & newSize, sizeof(newSize));
								try
								{
									persistent_containers::BufferView mb_new_size(free_spaces_by_size->get(mb_newSize));
									subtree_pos = dynamic_cast<persistent_containers::storedInsideAccessor<positionT>*>(free_spaces_by_size->MyNodeFactory->iA.get())->get(mb_new_size);
								}
								catch (persistent_containers::EmptyTree e)
								{
									create_new = true;
								}
								catch (persistent_containers::IndexNotFound i)
								{
									create_new = true;
								}

								persistent_containers::BufferView mb_newNode_pos((char*) &subtree_pos, sizeof(subtree_pos));
								if (create_new)
								{
									subtree_pos = reserveSpace(sizeof(positionT));
									free_spaces_by_size->store(mb_newSize, mb_newNode_pos, true);
									free_spaces_by_pos->store(mb_newNode_pos, mb_newSize, true);
								}
								
								std::shared_ptr<persistent_containers::storedInsideComparableAccessor<positionT>> subtree_iA(std::make_shared<persistent_containers::storedInsideComparableAccessor<positionT>>());
								std::shared_ptr<persistent_containers::storedInsideAccessor<positionT>> subtree_vA(std::make_shared<persistent_containers::storedInsideAccessor<positionT>>());
								persistent_containers::Tree<positionT> subtree(this->shared_from_this(), subtree_pos, subtree_iA, subtree_vA);
								positionT dummy = 0;
								persistent_containers::BufferView mb_dummy((char*)&dummy, sizeof(dummy));
								subtree.store(mb_newNode_pos, mb_dummy, true);
							}
							pos_pos_tree.remove((*first).first);
							free_spaces_by_pos->remove((*first).first);
						}
					}
					catch (persistent_containers::EmptyTree e) // no freed blocks recorded
					{}
				} // size_iter != end  -->  use physical_end, pos already is physical_end
				else 
				{}
			}
			catch (persistent_containers::EmptyTree i)
			{}

			if (pos == physical_end) // extend file if necessary
			{
				std::shared_ptr<persistent_storage::AbstractBuffer> buf = std::make_shared<persistent_storage::Buffer>(size);
				physical_end += size;
				file.seekp(pos);
				std::memset(buf->get_ptr(), 0, size);
				file.write(buf->get_ptr(), size);
				if (file.bad())
				{
					throw std::runtime_error("file bad() in reserveSpace()");
				}
				cache[pos] = buf;
			}
			return pos;
		}


		/** frees space in file
		*/
		void removeSpaceReservation(
			positionT pos, ///< position to free
			positionT size ///< size to free
		)
		{
			// set reminder for overwriting freed area
			std::shared_ptr<persistent_storage::Buffer> wipe_buffer = std::make_shared<persistent_storage::Buffer>(size); // zeroed in constructor
			wipe_buffer->mark_dirty();
			cache[pos] = wipe_buffer;

			auto iA(dynamic_cast<persistent_containers::storedInsideComparableAccessor<positionT>*>(free_spaces_by_pos->MyNodeFactory->iA.get()));
			auto vA(dynamic_cast<persistent_containers::storedInsideAccessor<positionT>*>(free_spaces_by_pos->MyNodeFactory->vA.get()));
			char *buf1 = new char[free_spaces_by_pos->MyNodeFactory->iA->getBytesToRead()];
			char *buf2 = new char[free_spaces_by_pos->MyNodeFactory->iA->getBytesToRead()];
			char *buf3 = new char[free_spaces_by_pos->MyNodeFactory->vA->getBytesToRead()];
			persistent_containers::BufferView mb_i(buf1, free_spaces_by_pos->MyNodeFactory->iA->getBytesToRead());
			persistent_containers::BufferView mb_ri(buf2, free_spaces_by_pos->MyNodeFactory->iA->getBytesToRead());
			persistent_containers::BufferView mb_rv(buf3, free_spaces_by_pos->MyNodeFactory->vA->getBytesToRead());

			auto pos_iter(free_spaces_by_size->end());

			iA->set(mb_i, pos);

			try
			{
				auto iter(free_spaces_by_pos->get_closest(mb_i));

				auto iter2(iter);
				auto iter3(iter);
				iter2--;
				iter3++;

				positionT pos1 = -1;
				positionT pos2 = -1;
				positionT pos3 = -1;
				positionT r1 = -1;
				positionT r2 = -1;
				positionT r3 = -1;

				if (iter != free_spaces_by_pos->end())
				{
					std::pair<persistent_containers::BufferView, persistent_containers::BufferView> result1(*iter);
					pos1 = iA->get(result1.first);
					r1 = vA->get(result1.second);
				}
				if (iter2 != free_spaces_by_pos->end())
				{
					std::pair<persistent_containers::BufferView, persistent_containers::BufferView> result2(*iter2);
					pos2 = iA->get(result2.first);
					r2 = vA->get(result2.second);
				}
				if (iter3 != free_spaces_by_pos->end())
				{
					std::pair<persistent_containers::BufferView, persistent_containers::BufferView> result3(*iter3);
					pos3 = iA->get(result3.first);
					r3 = vA->get(result3.second);
				}


				if (pos1 != -1 && ((pos1 >= pos && pos1 < pos + size || pos1 + r1 > pos && pos1 + r1 < pos + size) || (pos1 <= pos && pos1 + r1 > pos)))
				{
					throw std::logic_error("area with double free");
				}
				if (pos2 != -1 && ((pos2 >= pos && pos2 < pos + size || pos2 + r2 > pos && pos2 + r2 < pos + size) || (pos2 <= pos && pos2 + r2 > pos)))
				{
					throw std::logic_error("area with double free");
				}
				if (pos3 != -1 && ((pos3 >= pos && pos3 < pos + size || pos3 + r3 > pos && pos3 + r3 < pos + size) || (pos3 <= pos && pos3 + r3 > pos)))
				{
					throw std::logic_error("area with double free");
				}

				positionT result_pos = pos;
				positionT result_size;
				result_size = size;

				//compact adjacent entries
				bool adjust1 = false, adjust2 = false, adjust3 = false, adjust4 = false;
				if (pos1 + r1 == pos - 1)
				{
					result_pos = pos1;
					result_size = r1 + size;
					adjust1 = true;
				}
				if (pos2 == pos + size - 1)
				{
					if (adjust1)
					{
						result_size += r2;
						adjust2 = true;
					}
					else
					{
						result_pos = pos;
						result_size = r2 + size;
						adjust2 = true;
					}
				}

				if (pos2 + r2 == pos - 1)
				{
					result_pos = pos2;
					result_size = r2 + size;
					adjust3 = true;
				}
				if (pos3 == pos + size - 1)
				{
					if(adjust1)
					{
						result_size += r3;
						adjust4 = true;
					}
					else
					{
						result_pos = pos;
						result_size = r3 + size;
						adjust4 = true;
					}
				}

				iA->set(mb_ri, result_pos);
				vA->set(mb_rv, result_size);

				pos_iter = free_spaces_by_pos->store(mb_ri, mb_rv, true);
				if (adjust2)
				{
					persistent_containers::BufferView tmp1((char*) & pos2, sizeof(pos2));
					free_spaces_by_pos->remove(tmp1);
				}
				if (adjust4 && ! adjust1)
				{
					persistent_containers::BufferView tmp1((char*)&pos3, sizeof(pos2));
					free_spaces_by_pos->remove(tmp1);
				}
				if (adjust1 || adjust2 || adjust3 || adjust4)
					return;

				auto size_iA(dynamic_cast<persistent_containers::storedInsideComparableAccessor<positionT>*>(free_spaces_by_size->MyNodeFactory->iA.get()));
				auto size_vA(dynamic_cast<persistent_containers::storedInsideAccessor<positionT>*>(free_spaces_by_size->MyNodeFactory->vA.get()));
				char* buf_size = new char[free_spaces_by_pos->MyNodeFactory->vA->getBytesToRead()];
				persistent_containers::BufferView mb_size(buf_size, free_spaces_by_pos->MyNodeFactory->iA->getBytesToRead());
				size_iA->set(mb_size, size);

				std::shared_ptr<persistent_containers::storedInsideComparableAccessor<positionT>> pos_pos_iA = std::make_shared<persistent_containers::storedInsideComparableAccessor<positionT>>();
				std::shared_ptr<persistent_containers::storedInsideAccessor<positionT>> pos_pos_vA = std::make_shared<persistent_containers::storedInsideAccessor<positionT>>(); // TODO: replace by null-accessor later on.

				try
				{
					auto size_iter = free_spaces_by_size->get_closest(mb_size);

					positionT position_of_subtree( pos_pos_iA->get((*size_iter).second));
					positionT returned_size(pos_pos_iA->get((*size_iter).first));
					if (returned_size != size)
					{
						positionT reserve_for_tree = physical_end;//reserveSpace(sizeof(positionT)); // endless recursion
						physical_end += sizeof(positionT);
						persistent_containers::BufferView mb_tree((char *) & reserve_for_tree, sizeof(reserve_for_tree));
						free_spaces_by_size->store(mb_size, mb_tree, true);
						position_of_subtree = reserve_for_tree;
					}
					persistent_containers::Tree<positionT> position_set(this->shared_from_this(), position_of_subtree, pos_pos_iA, pos_pos_vA);
					
					positionT to_save = pos;//pos_iter.get_node_ptr()->get_pos();
					persistent_containers::BufferView mb_pos_pos((char*)  & to_save, sizeof(to_save));

					position_set.store(mb_pos_pos, mb_pos_pos, true);
				}
				catch (persistent_containers::EmptyTree e)
				{
					positionT reserve_for_tree = physical_end; //reserveSpace(sizeof(positionT)); //endless recursion
					physical_end += sizeof(positionT);
					persistent_containers::BufferView mb_tree((char*)&reserve_for_tree, sizeof(reserve_for_tree));
					free_spaces_by_size->store(mb_size, mb_tree, true);
					positionT position_of_subtree = reserve_for_tree;

					persistent_containers::Tree<positionT> pos_pos_tree(this->shared_from_this(), position_of_subtree, pos_pos_iA, pos_pos_vA);

					positionT to_save = pos;// pos_iter.get_node_ptr()->get_pos();
					persistent_containers::BufferView mb_pos_pos((char*)&to_save, sizeof(to_save));

					pos_pos_tree.store((*pos_iter).first, mb_pos_pos, true);
				}
				delete[] buf_size;
			}
			catch (persistent_containers::EmptyTree e)
			{
				iA->set(mb_ri, pos);
				vA->set(mb_rv, size);
				free_spaces_by_pos->store(mb_ri, mb_rv, true);
			}

			delete[] buf1;
			delete[] buf2;
			delete[] buf3;
		}


	private:
		std::fstream &file;
		positionT physical_end;
		std::map<positionT, std::shared_ptr<persistent_storage::AbstractBuffer>> cache;
		std::shared_ptr<persistent_containers::Tree<positionT>> free_spaces_by_pos;
		std::shared_ptr<persistent_containers::Tree<positionT>> free_spaces_by_size;
	};
}
#endif
