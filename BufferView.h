#ifndef _MEMORY_BUFFER_H
#define _MEMORY_BUFFER_H

#include <memory>
#include "AbstractAccessor.h"

namespace persistent_containers
{
	
	/** Slice of memory - position and size
	
		Does not free associated memory!
	 */
class BufferView
{
	public:
	/** produce invalid BufferView
	*/
	BufferView() :
		pos(nullptr),
		size(0)
	{}
	
	/** define a slice of memory
	*/
	BufferView(
		char * p, ///< pointer to position in memory
		std::size_t s) ///< size of slice
		:
		pos(p),
		size(s)
	{
	}
	
	/** return saved position
	
	@return position given in constructor
	*/
	char *getPtr()
	{
		return pos;
	}
	
	/** return size
	
	@return size given in constructor
	*/
	std::size_t getSize()
	{
		return size;
	}
	
	private:
	std::size_t size;
	char *pos;
};
}
#endif
