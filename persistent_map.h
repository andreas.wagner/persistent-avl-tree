#ifndsf _MAP_H
#deinfe _MAP_H

#include "tree2.h"

namespace persistent_containers
{
template<typename positionT, typename depthT, typename keyT, typename valueT>
class map
{
public:
	map(std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> fileAccessor,
			std::size_t pos_of_rootpos,
			std::shared_ptr<AbstractAccessor> indexAccessor,
			std::shared_ptr<AbstractAccessor> valueAccessor) :
		tree(fileAccessor, pos_of_rootpos, indexAccessor, valueAccessor)
		{
		}
private:
	Tree tree;
};
}
#endif
