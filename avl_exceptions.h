#ifndef AVL_EXCEPTIONS_H
#define AVL_EXCEPTIONS_H

#include <exception>
namespace persistent_containers
{
	class IndexNotFound : public std::exception
	{
	public:
		IndexNotFound()
		{
		}
		~IndexNotFound()
		{
		}
		const char* what()
		{
            return "Index not found";
		}
	private:
	};

	class IndexAlreadyExists : public std::exception
	{
	public:
		IndexAlreadyExists()
		{
		}
		~IndexAlreadyExists()
		{
		}
		const char* what()
		{
			return "Index already exists";
		}
	};

	class EmptyTree : public std::exception
	{
	public:
		EmptyTree()
		{
		}
	};
}
#endif
