#ifndef NODE_H
#define NODE_H

#include <cstddef>
#include "AbstractFileAccesor.h"

namespace persistent_containers
{
	template<typename positionT, typename indexA, typename valueA>
	class Node
	{
	public:
		Node(positionT pos, std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> file, indexA indexAccessor, valueA valueAccessor);

		//static constexpr std::size_t size();

		/*static std::size_t size()
		{
			indexA iA{};
			valueA vA{};
			return sizeof(positionT) * 3 + +sizeof(char) /*balance*//* +iA.getBytesToRead() + vA.getBytesToRead();
		}*/

		inline positionT get_left();

		inline positionT get_right();

		inline positionT get_parent();

		inline char get_balance();


		inline void set_left(positionT pos);

		inline void set_right(positionT pos);

		inline void set_parent(positionT pos);

		inline void set_balance(char bal);

		inline positionT get_pos();

		inline typename indexA::type getIndex();

		inline typename valueA::type getValue();

		inline void setIndex(typename indexA::type idx);

		inline void setValue(typename valueA::type val);

		//Node<positionT, indexA, valueA> operator=(Node<positionT, indexA, valueA> other);

		static std::size_t getBytesToRead();

		std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> get_file();

		indexA get_iA();

		valueA get_vA();


	private:
		std::shared_ptr<persistent_storage::AbstractBuffer> buf;
		std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> _file;
		valueA vA;
		indexA iA;
		positionT _pos;
	}; // class Node
}



namespace persistent_containers
{
	template<typename positionT, typename indexA, typename valueA>
	Node<positionT, indexA, valueA>::Node(positionT pos, std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> file, indexA indexAccessor, valueA valueAccessor) :
		buf(file->getFromPos(pos, indexAccessor.getBytesToRead()+valueAccessor.getBytesToRead()+getBytesToRead())),
		_file(file),
		_pos(pos),
		iA(indexAccessor),
		vA(valueAccessor)
	{
	}

	

	template<typename positionT, typename indexA, typename valueA>
	inline positionT Node<positionT, indexA, valueA>::get_left()
	{
		return *((positionT*)(buf->get_ptr() + sizeof(positionT) * 2));
	}

	template<typename positionT, typename indexA, typename valueA>
	inline positionT Node<positionT, indexA, valueA>::get_right()
	{
		return *((positionT*)(buf->get_ptr() + sizeof(positionT)));
	}

	template<typename positionT, typename indexA, typename valueA>
	inline positionT Node<positionT, indexA, valueA>::get_parent()
	{
		return *((positionT*)buf->get_ptr());
	}

	template<typename positionT, typename indexA, typename valueA>
	inline char Node<positionT, indexA, valueA>::get_balance()
	{
		return *((char*)buf->get_ptr() + sizeof(positionT) * 3);
	}


	template<typename positionT, typename indexA, typename valueA>
	inline void Node<positionT, indexA, valueA>::set_left(positionT pos)
	{
		*((positionT*)(buf->get_ptr() + sizeof(positionT) * 2)) = pos;
		buf->mark_dirty();
	}

	template<typename positionT, typename indexA, typename valueA>
	inline void Node<positionT, indexA, valueA>::set_right(positionT pos)
	{
		*((positionT*)(buf->get_ptr() + sizeof(positionT))) = pos;
		buf->mark_dirty();
	}

	template<typename positionT, typename indexA, typename valueA>
	inline void Node<positionT, indexA, valueA>::set_parent(positionT pos)
	{
		*((positionT*)buf->get_ptr()) = pos;
		buf->mark_dirty();
	}

	template<typename positionT, typename indexA, typename valueA>
	inline void Node<positionT, indexA, valueA>::set_balance(char bal)
	{
		*((char*)buf->get_ptr() + sizeof(positionT) * 3) = bal;
		buf->mark_dirty();
	}

	template<typename positionT, typename indexA, typename valueA>
	inline positionT Node<positionT, indexA, valueA>::get_pos()
	{
		return _pos;
	}


	template<typename positionT, typename indexA, typename valueA>
	inline typename indexA::type Node<positionT, indexA, valueA>::getIndex()
	{
		return iA.getElementFrom(buf->get_ptr() + sizeof(positionT) * 3 + sizeof(char));
	}

	template<typename positionT, typename indexA, typename valueA>
	inline typename valueA::type Node<positionT, indexA, valueA>::getValue()
	{
		return vA.getElementFrom(buf->get_ptr() + sizeof(positionT) * 3 + +sizeof(char) + iA.getBytesToRead());
	}

	template<typename positionT, typename indexA, typename valueA>
	inline void Node<positionT, indexA, valueA>::setIndex(typename indexA::type idx)
	{
		buf->mark_dirty();
		iA.putElementTo(buf->get_ptr() + sizeof(positionT) * 3 + sizeof(char), idx);
	}

	template<typename positionT, typename indexA, typename valueA>
	inline void Node<positionT, indexA, valueA>::setValue(typename valueA::type val)
	{
		buf->mark_dirty();
		vA.putElementTo(buf->get_ptr() + sizeof(positionT) * 3 + +sizeof(char) + iA.getBytesToRead(), val);
	}

	/*
	template<typename positionT, typename indexA, typename valueA>
	inline Node<positionT, indexA, valueA> Node<positionT, indexA, valueA>::operator=(Node<positionT, indexA, valueA> other)
	{
		iA = other.iA;
		vA = other.vA;
		buf = other.buf;
		_file = other._file;
		_pos = other._pos;
		return *this;
	}
	*/

	template<typename positionT, typename indexA, typename valueA>
	inline std::size_t Node<positionT, indexA, valueA>::getBytesToRead()
	{
		return sizeof(positionT) * 3 + sizeof(char); // pointers to left, right and parent aswell as balance
	}

	template<typename positionT, typename indexA, typename valueA>
	inline std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> Node<positionT, indexA, valueA>::get_file()
	{
		return _file;
	}

	template<typename positionT, typename indexA, typename valueA>
	inline indexA Node<positionT, indexA, valueA>::get_iA()
	{
		return iA;
	}

	template<typename positionT, typename indexA, typename valueA>
	inline valueA Node<positionT, indexA, valueA>::get_vA()
	{
		return vA;
	}


}

#endif