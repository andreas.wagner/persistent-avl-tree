#ifndef BUFFER_H
#include "AbstractBuffer.h"
#include <cstddef>
#include <memory>

namespace persistent_storage
{
	/** managed buffer
	*/
	class Buffer : public AbstractBuffer
	{
	public:
		Buffer() = delete;

		/** allocate buffer of specific size
		*/
		Buffer(
			std::size_t size ///< size of buffer to allocate
		);
		
		/** releases buffer
		*/
		~Buffer();
		
		/** reveal pointer
		@return pointer to buffer
		*/
		char* get_ptr() override;
		
		/** return size of buffer
		@return size of buffer
		*/
		constexpr std::size_t get_size() override;


	private:
		std::unique_ptr<char[]> _ptr;
		std::size_t _size;
	}; // class Buffer
}

namespace persistent_storage
{
	Buffer::Buffer(std::size_t size) :
		_ptr(new char[size]),
		_size(size)
	{
		std::memset(_ptr.get(), 0, size);
	}

	Buffer::~Buffer()
	{
		//delete[] _ptr.get();
	}

	char* Buffer::get_ptr()
	{
		return _ptr.get();
	}

	constexpr std::size_t Buffer::get_size()
	{
		return _size;
	}
}
#endif
