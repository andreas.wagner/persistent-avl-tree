#ifndef ABSTRACTACCESSOR_H
#define ABSTRACTACCESSOR_H

#include "BufferView.h"

namespace persistent_containers
{

		
	/** interface for accessing memory buffers
	*/
	class AbstractAccessor
	{
	public:

		/** returns number of bytes to read from node
		@return number of bytes to read from node
		*/
		virtual std::size_t getBytesToRead()
		{
			return 0;
		}

		/** fetch element from position
		*/
		virtual BufferView getElementFrom(
			char* pos ///< storage-position in node
			)
		{
			return BufferView();
		}

		/** put element to position
		*/
		virtual constexpr void putElementTo(
			char* pos, ///< position in memory; has to be in an node
			BufferView& elem ///< memory slice holding the entry
			)
		{}
		


	};
}

#endif
