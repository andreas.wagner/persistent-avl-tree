#include <list>
#include <cstdint>
#include <cstring>
#include <cstdlib>
#include <memory>
#include <iostream>
#include "transaction-layer.h"

#include "forward-list.h"
#include "FreespaceManagingFileAccessor.h"

int main(void)
{
	std::string tree_filename("transact-test.dat");
	std::fstream file(tree_filename, std::ios_base::out | std::ios_base::trunc);

	file.seekp(0);
	
	file.write("1234567_1234567_", sizeof(std::size_t) * 2);
	if (file.bad())
	{
		throw std::runtime_error("file bad()");
	}


	std::shared_ptr<persistent_containers::Tree<std::size_t>> tree;

	std::shared_ptr<persistent_storage::FreespaceManagingFileAccessor<std::size_t>> file_acc
		= std::make_shared<persistent_storage::FreespaceManagingFileAccessor<std::size_t>>(file);
	file_acc->initialize(0);
	std::size_t pos = file_acc->reserveSpace(sizeof(std::size_t));

	std::shared_ptr<persistent_containers::storedInsideComparableAccessor<std::int32_t>> tree_indexAccessor = std::make_shared<persistent_containers::storedInsideComparableAccessor<std::int32_t>>();
	std::shared_ptr<persistent_containers::storedInsideAccessor<std::int64_t>> tree_valueAccessor = std::make_shared<persistent_containers::storedInsideAccessor<std::int64_t>>();
	
	tree = std::make_shared<persistent_containers::Tree<std::size_t>>(file_acc, pos, tree_indexAccessor, tree_valueAccessor);

	tree->set_root_pos(0);
	file_acc->flushCache();

	std::shared_ptr<persistent_containers::storedInsideAccessor<std::int32_t>> list_valueAccessor = std::make_shared<persistent_containers::storedInsideAccessor<std::int32_t>>();
	persistent_containers::forward_list<std::size_t> my_list(file_acc, file_acc->reserveSpace(sizeof(std::size_t)*2), list_valueAccessor);



	std::list<int> input_values{ 9, 8, 1, 2, 7, 6, 3, 4, 5, 14, 11, 13, 12, 10 };
	std::list<int> to_remove_values{ 7, 12, 13, 14, 8, 9, 2, 1, 5, 3, 4, 11, 6, 10 };

	int index = 0;

	for (auto& i : input_values)
	{
		char test1[4];
		char test2[8];
		persistent_containers::BufferView buf1(test1, 32/8);
		persistent_containers::BufferView buf2(test2, 64/8);
		tree_indexAccessor->set(buf1, i);
		tree_valueAccessor->set(buf2, i);
		try
		{
			tree->store(buf1, buf2, false);
		}
		catch (std::runtime_error& err)
		{
			std::cerr << err.what();
			return -1;
		}
		catch (std::logic_error& err)
		{
			std::cerr << err.what();
			return -1;
		}
		test_avl(tree->get_root_pos(), *tree);

	}
	file_acc->flushCache();
	file_acc->flush();
				
	auto end = tree->end();
	auto i = tree->begin();
	while(!(i == end))
	{
		std::cout << (long) *(*i).first.getPtr() << " ";
		i++;
	}
	std::cout << std::endl;
	
	for (auto& i : to_remove_values)
	{
		char test1[4];
		persistent_containers::BufferView buf1(test1, 32/8);
		tree_indexAccessor->set(buf1, i);
		try
		{
			tree->remove(buf1);
		}
		catch (std::runtime_error& err)
		{
			std::cerr << err.what();
			return -1;
		}
/*		catch (std::logic_error& err)
		{
			std::cerr << err.what();
			return -1;
		}*/
		test_avl(tree->get_root_pos(), *tree);
	}
	
	file_acc->flushCache();
	file_acc->flush();
		
	srand(1);
	for (int i = 0; i < 200; i++)
	{
		std::int32_t r = rand();
		char test1[4];
		char test2[8];
		persistent_containers::BufferView buf1(test1, 32/8);
		persistent_containers::BufferView buf2(test2, 64/8);
		tree_indexAccessor->set(buf1, r);
		tree_valueAccessor->set(buf2, r);
		try
		{
			tree->store(buf1, buf2, false);
			test_avl(tree->get_root_pos(), *tree);
		}
		catch (std::runtime_error& err)
		{
			std::cerr << err.what();
			return -1;
		}
		catch (std::logic_error& err)
		{
			std::cerr << err.what();
			return -1;
		}
	}
	file_acc->flushCache();
	file_acc->flush();
	
	srand(2);
	for (int i = 0; i < 200; i++)
	{
		bool try_other = false;
		std::int32_t r = rand();
		char test1[4];
		persistent_containers::BufferView buf1(test1, 4);
		tree_indexAccessor->set(buf1, r);
		try
		{
			auto iter = persistent_containers::Tree<std::size_t>::iterator(buf1, false, *tree);
			tree->remove(buf1);
			test_avl(tree->get_root_pos(), *tree);
		}
		catch (std::runtime_error& err)
		{
			std::cerr << err.what();
			return -1;
		}
		catch (std::logic_error& err)
		{
			std::cerr << err.what();
			return -1;
		}
		catch(persistent_containers::IndexNotFound i)
		{
			try_other = true;
		}
		if (try_other)
		{
			char test1[4];
			
			persistent_containers::BufferView buf2(test1, 4);
			tree_indexAccessor->set(buf1, r);
			
			auto iter = persistent_containers::Tree<std::size_t>::iterator(buf2, true, *tree);
			if(iter != tree->end())
			{
				tree->remove((*iter).first);
			}
		}
	}
	file_acc->flushCache();
	file_acc->flush();
	
	/*
	for (int i = 0; i < 10000; i++)
	{
		my_list.push_back(i);
	}
	file_acc->flushCache();
	file_acc->flush();
	for (auto  le : my_list)
	{
		std::cout << le.get_value() << " ";
	}
	*/
}
