#ifndef FORWARD_LIST_H
#define FORWARD_LIST_H

#include <algorithm>
#include "AbstractFileAccesor.h"
#include "BufferView.h"

namespace persistent_containers
{

	template<typename positionT>
	class forward_list
	{
	public:
		forward_list(std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> fileAccessor, positionT anchor_pos, std::shared_ptr<persistent_containers::AbstractAccessor> valueAccessor);

		void push_back(BufferView value);

		class list_element
		{
		public:
			list_element(positionT pos, BufferView value, std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> fileAccessor, std::shared_ptr<persistent_containers::AbstractAccessor> valueAccessor) :
				element_pos(pos),
				_file(fileAccessor),
				buf(fileAccessor->getFromPos(pos, valueAccessor->getBytesToRead() + sizeof(positionT))),
				vA(valueAccessor)
			{
				vA->putElementTo(buf->get_ptr() + sizeof(positionT), value);
				buf->mark_dirty();
				// expect buffer to be memset to 0, so no need to put a zero into the "next"-slot.
			}
			list_element(positionT pos, std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> fileAccessor, BufferView valueAccessor) :
				element_pos(pos),
				vA(valueAccessor),
				_file(fileAccessor),
				buf(nullptr)
			{
				if (pos != 0) // necessary for end()-iterator
					buf = fileAccessor->getFromPos(pos, vA->getBytesToRead() + sizeof(positionT));
			}
			BufferView get_value();
			//void set_value(valueA::type val);
			positionT get_next();
			void set_next(positionT pos);
			bool operator==(list_element other);
			positionT get_pos() { return element_pos; };
		private:
			positionT element_pos;
			std::shared_ptr<persistent_storage::AbstractBuffer> buf;
			std::shared_ptr<persistent_containers::AbstractAccessor> vA;
			std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> _file;
		};

		class iterator : public std::iterator<std::forward_iterator_tag, list_element, list_element, const list_element*, list_element>
		{
		public:
			explicit iterator(positionT pos, std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> fileAccessor, std::shared_ptr<persistent_containers::AbstractAccessor> valueAccessor) :
				listElement(),
				_file(fileAccessor),
				vA(valueAccessor)
			{
				listElement = std::make_shared<list_element>(pos, _file, vA);
			}
			iterator& operator++() { listElement = std::make_shared<list_element>(listElement->get_next(), _file, vA); return *this; }
			iterator operator++( int a ) { iterator retval = *this; ++(*this); return retval; }
			bool operator==(iterator other) const { return listElement->get_pos() == other.listElement->get_pos(); }
			bool operator!=(iterator other) const { return !(*this == other); }
			list_element operator*() const { return *(listElement.get()); }
		private:
			std::shared_ptr<list_element> listElement;
			std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> _file;
			std::shared_ptr<persistent_containers::AbstractAccessor> vA;
		};
		iterator begin() { return iterator(get_start(), _file, vA); }
		iterator end() { return iterator(0, _file, vA); }

	private:
		
		positionT get_start();
		positionT get_end();
		void set_start(positionT pos);
		void set_end(positionT pos);
		std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> _file;
		positionT start_end_pos;
		std::shared_ptr<persistent_containers::AbstractAccessor> vA;
	};


}


namespace persistent_containers
{
	template<typename positionT>
	forward_list<positionT>::forward_list(std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> fileAccessor, positionT pos, std::shared_ptr<persistent_containers::AbstractAccessor> valueAccessor) :
		start_end_pos(pos),
		_file(fileAccessor),
		vA(valueAccessor)
	{
		set_start(0);
		set_end(0);
	}

	template<typename positionT>
	void forward_list<positionT>::push_back(BufferView value)
	{
		list_element le(_file->reserveSpace(vA->getBytesToRead() + sizeof(positionT)), value,  _file, vA);
		

		if (get_start() == 0)
		{
			set_start(le.get_pos());
			set_end(le.get_pos());
		}
		else
		{
			list_element end_element(get_end(), _file, vA);
			end_element.set_next(le.get_pos());
			set_end(le.get_pos());
		}
	}

	template<typename positionT>
	BufferView forward_list<positionT>::list_element::get_value()
	{
		std::shared_ptr<persistent_storage::AbstractBuffer> buf = _file->getFromPos(element_pos, sizeof(positionT)+vA->getBytesToRead());
		return vA->getElementFrom(buf->get_ptr()+sizeof(positionT));
	}

	template<typename positionT>
	bool forward_list<positionT>::list_element::operator==(list_element other)
	{
		return element_pos == other.element_pos;
	}

	template<typename positionT>
	positionT forward_list<positionT>::list_element::get_next()
	{
		return *((positionT*) buf->get_ptr());
	}

	template<typename positionT>
	void forward_list<positionT>::list_element::set_next(positionT pos)
	{
		*((positionT*)buf->get_ptr()) = pos;
		buf->mark_dirty();
	}

	template<typename positionT>
	positionT forward_list<positionT>::get_start()
	{
		std::shared_ptr<persistent_storage::AbstractBuffer> buf = _file->getFromPos(start_end_pos, sizeof(positionT)*2);
		positionT pos = *((positionT*)(buf->get_ptr()));;
		return pos;
	}

	template<typename positionT>
	positionT forward_list<positionT>::get_end()
	{
		std::shared_ptr<persistent_storage::AbstractBuffer> buf = _file->getFromPos(start_end_pos, sizeof(positionT)*2);
		return *((positionT*)(buf->get_ptr()+sizeof(positionT)));
	}

	template<typename positionT>
	void forward_list<positionT>::set_start(positionT pos)
	{
		std::shared_ptr<persistent_storage::AbstractBuffer> buf = _file->getFromPos(start_end_pos, sizeof(positionT)*2);
		*((positionT*)(buf->get_ptr())) = pos;
		buf->mark_dirty();
	}

	template<typename positionT>
	void forward_list<positionT>::set_end(positionT pos)
	{
		std::shared_ptr<persistent_storage::AbstractBuffer> buf = _file->getFromPos(start_end_pos, sizeof(positionT)*2);
		*((positionT*)(buf->get_ptr()+sizeof(positionT))) = pos;
		buf->mark_dirty();
	}
}
#endif
