#ifndef TREE_H
#define TREE_H

#include "AbstractFileAccesor.h"
#include "AbstractBuffer.h"
#include "AbstractAccessor.h"

#include <iterator>

namespace persistent_containers
{
	template<typename positionT, typename indexA, typename valueA>
	class Tree
	{
	public:
		Tree(std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> fileAccessor, std::size_t pos_of_rootpos, indexA indexAccessor, valueA valueAccessor) :
			_file(fileAccessor),
			_pos_of_rootpos(pos_of_rootpos),
			iA(indexAccessor),
			vA(valueAccessor)
		{
		}

		class iterator : public std::iterator<
			std::bidirectional_iterator_tag,
			std::pair<typename indexA::type, typename valueA::type>,
			std::pair<typename indexA::type, typename valueA::type>,
			const std::pair<typename indexA::type, typename valueA::type>*,
			std::pair<typename indexA::type, typename valueA::type>>
		{
		public:
			explicit iterator(indexA::type index, bool use_closest, Tree t) :
				current_node(std::make_shared<Node<positionT, indexA, valueA>>(t.find(index, use_closest)))
			{}
			explicit iterator(positionT pos, Tree t) :
				current_node(std::make_shared<Node<positionT, indexA, valueA>>(pos, t.get_file(), t.get_iA(), t.get_vA()))
			{
			}
			explicit iterator() :
				current_node()
			{}
			iterator& operator++()
			{
				if (current_node->get_right() != 0)
				{
					current_node = std::make_shared<Node<positionT, indexA, valueA>>(current_node->get_right(), current_node->get_file(), current_node->get_iA(), current_node->get_vA());
					while (current_node->get_left() != 0)
					{
						current_node = std::make_shared<Node<positionT, indexA, valueA>>(current_node->get_left(), current_node->get_file(), current_node->get_iA(), current_node->get_vA());
					}
					return *this;
				}
				else
				{
					positionT former_pos = current_node->get_pos();
					do
					{
						former_pos = current_node->get_pos();
						if (current_node->get_parent() != 0)
						{
							current_node = std::make_shared<Node<positionT, indexA, valueA>>(current_node->get_parent(), current_node->get_file(), current_node->get_iA(), current_node->get_vA());
							
						}
						else
						{
							current_node.reset();
							return *this;
						}
					} while (current_node->get_left() != former_pos);
				}
				return *this;
			}

			iterator& operator--()
			{
				if (current_node->get_left() != 0)
				{
					current_node = std::make_shared<Node<positionT, indexA, valueA>>(current_node->get_left(), current_node->get_file(), current_node->get_iA(), current_node->get_vA());
					while (current_node->get_right() != 0)
					{
						current_node = std::make_shared<Node<positionT, indexA, valueA>>(current_node->get_right(), current_node->get_file(), current_node->get_iA(), current_node->get_vA());
					}
					return *this;
				}
				else
				{
					positionT former_pos = current_node->get_pos();
					do
					{
						former_pos = current_node->get_pos();
						if (current_node->get_parent() != 0)
						{
							current_node = std::make_shared<Node<positionT, indexA, valueA>>(current_node->get_parent(), current_node->get_file(), current_node->get_iA(), current_node->get_vA());
						}
						else
						{
							current_node.reset();
							return *this;
						}
					} while (current_node->get_right() != former_pos);
				}
			}

			bool operator==(iterator& other) const
			{
				if (other.current_node && current_node)
					return current_node->get_pos() == other.current_node->get_pos();
				else
					return (!other.current_node && !current_node);
			}

#ifdef _WIN32
			bool operator!=(iterator other) const { return !(*this == other); }
#endif

			std::pair<typename indexA::type, typename valueA::type> operator* ()
			{
				return std::pair<typename indexA::type, typename valueA::type>(current_node->getIndex(), current_node->getValue());
			}

			iterator operator++(int) { iterator retval = *this; ++(*this); return retval; }
			iterator operator--(int) { iterator retval = *this; --(*this); return retval; }

			std::shared_ptr<Node<positionT, indexA, valueA>> get_node_ptr()
			{
				return current_node;
			}
		private:

			std::shared_ptr<Node<positionT, indexA, valueA>> current_node;
		};

		iterator begin()
		{
			Node<positionT, indexA, valueA> current(get_root_pos(), _file, iA, vA);
			while (current.get_left() != 0)
				current = Node<positionT, indexA, valueA>(current.get_left(), _file, iA, vA);
			return iterator(current.get_pos(), *this);
		}
		iterator end()
		{
			return iterator();
		}

		void store(indexA::type index, valueA::type value)
		{
			try
			{
				Node<positionT, indexA, valueA> closest = find(index, true);

				if (closest.getIndex() == index)
				{
					throw IndexAlreadyExists<typename indexA::type>(index);
				}
				else
				{
					Node<positionT, indexA, valueA> newNode(_file->reserveSpace(iA.getBytesToRead() + vA.getBytesToRead() + Node<positionT, indexA, valueA>::getBytesToRead()), _file, iA, vA);
					newNode.setIndex(index);
					newNode.setValue(value);
					newNode.set_balance(0);
					newNode.set_parent(closest.get_pos());
					if (closest.getIndex() < index)
					{
						if (closest.get_right() == 0)
						{
							closest.set_right(newNode.get_pos());
						}
						else
						{
							throw std::runtime_error("This should never happen.");
						}
					}
					else if (closest.getIndex() > index)
					{
						if (closest.get_left() == 0)
						{
							closest.set_left(newNode.get_pos());
						}
						else
						{
							throw std::runtime_error("This should never happen.");
						}
					}
					rebalance(newNode, false);
				}

			}
			catch (EmptyTree& e)
			{
				Node<positionT, indexA, valueA> newNode(_file->reserveSpace(Node<positionT, indexA, valueA>::getBytesToRead() + iA.getBytesToRead() + vA.getBytesToRead()), _file, iA, vA);
				newNode.setIndex(index);
				newNode.setValue(value);
				set_root_pos(newNode.get_pos());
			}

		}

		void remove(indexA::type index)
		{
			iterator to_remove(index, false, *this);
			if (to_remove != end())
			{
				std::shared_ptr<Node<positionT, indexA, valueA>> to_del = to_remove.get_node_ptr();
				iterator rebalanceable = to_remove;
				rebalanceable++;
				
				positionT pos_to_rebalance;


				if (to_del->get_right() != 0 && to_del->get_left() != 0)
				{
					iterator to_append = to_remove;
					to_append++;
					std::shared_ptr<Node<positionT, indexA, valueA>> to_app = to_append.get_node_ptr();
					
					if (to_del->get_pos() == get_root_pos())
					{
						std::cerr << "A" << std::endl;
						pos_to_rebalance = rebalanceable.get_node_ptr()->get_parent();
						
						Node<positionT, indexA, valueA> app_parent(to_app->get_parent(), _file, iA, vA);
						if (app_parent.get_left() == to_app->get_pos())
						{
							app_parent.set_left(to_app->get_right());
						}
						else if (app_parent.get_right() == to_app->get_pos())
						{
							app_parent.set_right(to_app->get_right());
						}
						else
							throw std::logic_error("Here's something severely wrong!");
						if (to_app->get_right() != 0)
						{
							Node<positionT, indexA, valueA> app_right_child(to_app->get_right(), _file, iA, vA);
							app_right_child.set_parent(app_parent.get_pos());
						}

						//to_app->set_parent(to_del->get_parent());
							
						set_root_pos(to_app->get_pos());

						to_app->set_parent(0);
						to_app->set_right(to_del->get_right());
						to_app->set_left(to_del->get_left());
						Node<positionT, indexA, valueA> mod_l(to_del->get_left(), _file, iA, vA);
						Node<positionT, indexA, valueA> mod_r(to_del->get_right(), _file, iA, vA);
						mod_l.set_parent(to_app->get_pos());
						mod_r.set_parent(to_app->get_pos());
						
					}
					else if (to_app->get_pos() == get_root_pos())
					{

						std::cerr << "B" << std::endl;
						throw std::logic_error("huh? #4672");
					}
					else
					{
						std::cerr << "C" << std::endl;
						pos_to_rebalance = rebalanceable.get_node_ptr()->get_parent();
						
						std::shared_ptr<Node<positionT, indexA, valueA>> to_app = to_append.get_node_ptr();
						Node<positionT, indexA, valueA> app_parent(to_app->get_parent(), _file, iA, vA);
						
						if (to_app->get_right() != 0 && !(to_del->get_right() != to_app->get_pos()))
						{
							Node<positionT, indexA, valueA> app_right_child(to_app->get_right(), _file, iA, vA);
							app_right_child.set_parent(app_parent.get_pos());
						}

						Node<positionT, indexA, valueA> parent1(to_del->get_parent(), _file, iA, vA);
						to_app->set_parent(to_del->get_parent());
						if (parent1.get_left() == to_del->get_pos())
						{
							parent1.set_left(to_app->get_pos());
						}
						else if (parent1.get_right() == to_del->get_pos())
						{
							parent1.set_right(to_app->get_pos());
						}
						else
						{
							throw std::runtime_error("bad tree"); // Todo: more verbose debug-info
						}

						if (to_del->get_right() != to_app->get_pos())
						{
							if (app_parent.get_left() == to_app->get_pos())
							{
								app_parent.set_left(to_app->get_right());
							}
							else if (app_parent.get_right() == to_app->get_pos())
							{
								app_parent.set_right(to_app->get_right());
							}
							else
								throw std::logic_error("Here's something severely wrong!");
							to_app->set_right(to_del->get_right());
							to_app->set_left(to_del->get_left());
							Node<positionT, indexA, valueA> mod_l(to_del->get_left(), _file, iA, vA);
							Node<positionT, indexA, valueA> mod_r(to_del->get_right(), _file, iA, vA);
							mod_l.set_parent(to_app->get_pos());
							mod_r.set_parent(to_app->get_pos());
						}
						else
						{
							to_app->set_left(to_del->get_left());
							Node<positionT, indexA, valueA> mod_l(to_del->get_left(), _file, iA, vA);
							mod_l.set_parent(to_app->get_pos());
						}
					}
				}
				else // to_del->get_right() == 0 || to_del->get_left() == 0
				{
					if (to_del->get_left() != 0)
					{
						std::cerr << "D" << std::endl;
						pos_to_rebalance = to_del->get_left();
						
						if (to_del->get_pos() != get_root_pos())
						{
							Node<positionT, indexA, valueA> parent1(to_del->get_parent(), _file, iA, vA);

							if (parent1.get_left() == to_del->get_pos())
							{
								parent1.set_left(to_del->get_left());
								Node<positionT, indexA, valueA> mod_l(to_del->get_left(), _file, iA, vA);
								mod_l.set_parent(parent1.get_pos());
							}
							else if (parent1.get_right() == to_del->get_pos())
							{
								parent1.set_right(to_del->get_left());
								Node<positionT, indexA, valueA> mod_r(to_del->get_left(), _file, iA, vA);
								mod_r.set_parent(parent1.get_pos());
							}
							else
							{
								throw std::runtime_error("This error should be impossible.");
							}
						}
						else
						{
							set_root_pos(to_del->get_left());
							Node<positionT, indexA, valueA> mod_l(to_del->get_left(), _file, iA, vA);
							mod_l.set_parent(0);
						}
					}
					else if (to_del->get_right() != 0)
					{
						std::cerr << "E" << std::endl;
						pos_to_rebalance = to_del->get_right();
						
						if (to_del->get_pos() != get_root_pos())
						{
							Node<positionT, indexA, valueA> parent1(to_del->get_parent(), _file, iA, vA);

							Node<positionT, indexA, valueA> mod_(to_del->get_right(), _file, iA, vA);
							mod_.set_parent(parent1.get_pos());
							
							if (parent1.get_left() == to_del->get_pos())
							{
								parent1.set_left(to_del->get_right());
							}
							else if (parent1.get_right() == to_del->get_pos())
							{
								parent1.set_right(to_del->get_right());
							}
							else
							{
								throw std::runtime_error("This error should be impossible. #1");
							}
						}
						else
						{
							std::cerr << "F" << std::endl;
							set_root_pos(to_del->get_right());
						}
					}
					else
					{
						std::cerr << "G" << std::endl;
						pos_to_rebalance = to_del->get_parent();
						
						if (to_del->get_pos() != get_root_pos())
						{
							Node<positionT, indexA, valueA> parent1(to_del->get_parent(), _file, iA, vA);
							//parent1.set_left(0);
							/*if(to_del->get_right() != 0)
							{
								Node<positionT, indexA, valueA> mod_(to_del->get_right(), _file, iA, vA);
								mod_.set_parent(parent1.get_pos());
							}*/
							if (parent1.get_left() == to_del->get_pos())
							{
								parent1.set_left(0);
							}
							else if (parent1.get_right() == to_del->get_pos())
							{
								parent1.set_right(0);
							}
							else
							{
								throw std::runtime_error("This error should be impossible. #2");
							}
						}
						else
						{
							std::cerr << "H" << std::endl;
							set_root_pos(0);
						}
					}
				}
				_file->removeSpaceReservation(to_del->get_pos(), to_del->getBytesToRead());
				print_tree("1_test.dot");
				if(pos_to_rebalance != 0)
				{
					Node<positionT, indexA, valueA> to_rebalance = Node<positionT, indexA, valueA>(
						pos_to_rebalance
						, _file, iA, vA);
					rebalance(to_rebalance, true);
				}
			}
			// TODO: remove referenced information
//					valueA::getBytesToRead indexA::getBytesToRead
		}



		valueA::type get(indexA::type index)
		{
			return find(index, false).getValue();
		}

		positionT get_root_pos()
		{
			std::shared_ptr<persistent_storage::AbstractBuffer> buf = _file->getFromPos(_pos_of_rootpos, sizeof(positionT));
			return *((positionT*)buf->get_ptr());
		}

		void set_root_pos(positionT pos)
		{
			std::shared_ptr<persistent_storage::AbstractBuffer> buf = _file->getFromPos(_pos_of_rootpos, sizeof(pos));
			*((positionT*)buf->get_ptr()) = pos;
			buf->mark_dirty();
		}

		void print_tree(std::filesystem::path filename)
		{
			std::ofstream out(filename, std::ios_base::trunc | std::ios_base::out);
			std::cerr << "creating " << filename.string() << std::endl;
			out.flush();
			out << "digraph G\n{\n\tnode[shape=record];\n";
			out << "root[label=\"root|" << get_root_pos() << "\"]; \n";
			if (get_root_pos() != 0)
			{
				Node<positionT, indexA, valueA> start = Node<positionT, indexA, valueA>(get_root_pos(), _file, iA, vA);
				print_subtree(start, out);
			}
			out << "}\n";
			out.flush();
		}


		std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> get_file()
		{
			return _file;
		}

		valueA get_vA()
		{
			return vA;
		}

		indexA get_iA()
		{
			return iA;
		}

	private:

		void print_subtree(Node<positionT, indexA, valueA> n, std::ofstream& out)
		{

			typename indexA::type index = n.getIndex();
			typename indexA::type index_l;
			typename indexA::type index_r;
			typename indexA::type index_p;
			out << "p" << index << "[label=\"" << "p" << index << "," << n.get_pos();
			out << "|" << (int)n.get_balance();

			if (n.get_left() != 0)
			{
				typename indexA::type ind = n.getIndex();
				typename valueA::type val = n.getValue();
				positionT l = n.get_left();
				positionT r = n.get_right();
				positionT p = n.get_parent();
				positionT po = n.get_pos();
				Node<positionT, indexA, valueA> left_node(n.get_left(), _file, iA, vA);
				index_l = left_node.getIndex();
				out << "|lp" << index_l << "," << n.get_left();
			}
			else
			{
				out << "|-";
			}
			if (n.get_right() != 0)
			{
				Node<positionT, indexA, valueA> right_node(n.get_right(), _file, iA, vA);
				index_r = right_node.getIndex();
				out << "|rp" << index_r << "," << n.get_right();
			}
			else
			{
				out << "|-";
			}



			out << "\"];" << std::endl;

			if (n.get_left() != 0)
			{
				Node<positionT, indexA, valueA> left(n.get_left(), _file, iA, vA);
				print_subtree(left, out);
				index_l = left.getIndex();
				out << "p" << index << " -> " << "p" << index_l << "[label=\"l\"];" << std::endl;
			}
			if (n.get_right() != 0)
			{
				Node<positionT, indexA, valueA> right(n.get_right(), _file, iA, vA);
				print_subtree(right, out);
				index_r = right.getIndex();
				out << "p" << index << " -> " << "p" << index_r << "[label=\"r\"];" << std::endl;
			}
			if (n.get_parent() != 0)
			{
				Node<positionT, indexA, valueA> parent_node(n.get_parent(), _file, iA, vA);
				index_p = parent_node.getIndex();
				out << "p" << index << " -> p" << index_p << "[label=\"p\"];" << std::endl;
			}
			else
			{
				out << "p" << index << " -> root[label=\"p\"];";
			}

		}

		Node<positionT, indexA, valueA> find(indexA::type index, bool return_closest)
		{
			positionT root_pos = get_root_pos();
			if (root_pos == 0)
			{
				throw EmptyTree();
			}
			Node<positionT, indexA, valueA> n(root_pos, _file, iA, vA);
			while (n.getIndex() != index)
			{
				if (n.getIndex() > index)
				{
					if (n.get_left() != 0)
						n = Node<positionT, indexA, valueA>(n.get_left(), _file, iA, vA);
					else
						if (return_closest)
							return n;
						else
							throw IndexNotFound<typename indexA::type>(index);
				}
				else if (n.getIndex() < index)
				{
					if (n.get_right() != 0)
						n = Node<positionT, indexA, valueA>(n.get_right(), _file, iA, vA);
					else
						if (return_closest)
							return n;
						else
							throw IndexNotFound<typename indexA::type>(index);
				}
			}
			return n;
		}


		void rotateRight(Node<positionT, indexA, valueA>& n)
		{
			std::cerr << "Rotate right: " << n.getIndex() << std::endl;
			if (n.get_parent() == 0)
				throw std::runtime_error("(n.get_parent() == 0)");
			Node<positionT, indexA, valueA> parent(n.get_parent(), _file, iA, vA);
			parent.set_left(n.get_right());
			if (n.get_right() != 0)
			{
				Node<positionT, indexA, valueA> right_tree(n.get_right(), _file, iA, vA);
				right_tree.set_parent(parent.get_pos());
			}
			if (parent.get_parent() != 0)
			{
				Node<positionT, indexA, valueA> parents_parent(parent.get_parent(), _file, iA, vA);
				if (parents_parent.get_left() == parent.get_pos())
				{
					parents_parent.set_left(n.get_pos());
				}
				else
				{
					parents_parent.set_right(n.get_pos());
				}
				n.set_parent(parents_parent.get_pos());
			}
			else //(parent.get_parent() == 0)
			{ // adjust root
				set_root_pos(n.get_pos());
				n.set_parent(0);
			}
			parent.set_parent(n.get_pos());
			if (parent.get_balance() == 0)
			{
				parent.set_balance(1);
				n.set_balance(-1);
			}
			else
			{
				parent.set_balance(0);
				n.set_balance(0);
			}
			n.set_right(parent.get_pos());
		}

		void rotateLeft(Node<positionT, indexA, valueA>& n)
		{
			std::cerr << "Rotate left: " << n.getIndex() << std::endl;
			if (n.get_parent() == 0)
				throw std::runtime_error("(n.get_parent() == 0)");
			Node<positionT, indexA, valueA> parent(n.get_parent(), _file, iA, vA);
			parent.set_right(n.get_left());
			if (n.get_left() != 0)
			{
				Node<positionT, indexA, valueA> left_tree(n.get_left(), _file, iA, vA);
				left_tree.set_parent(parent.get_pos());
			}
			if (parent.get_parent() != 0)
			{
				Node<positionT, indexA, valueA> parents_parent(parent.get_parent(), _file, iA, vA);
				if (parents_parent.get_left() == parent.get_pos())
				{
					parents_parent.set_left(n.get_pos());
				}
				else
				{
					parents_parent.set_right(n.get_pos());
				}
				n.set_parent(parents_parent.get_pos());
			}
			else //(parent.get_parent() == 0)
			{ // adjust root
				set_root_pos(n.get_pos());
				n.set_parent(0);
			}
			parent.set_parent(n.get_pos());
			if (parent.get_balance() == 0) //
			{
				parent.set_balance(-1);
				n.set_balance(1);
			}
			else
			{
				parent.set_balance(0);
				n.set_balance(0);
			}
			n.set_left(parent.get_pos());
		}

		void rotateLeftRight(Node<positionT, indexA, valueA>& n)
		{
			std::cerr << "rotateLeftRight" << std::endl;
			Node<positionT, indexA, valueA> Z = n;
			Node<positionT, indexA, valueA> Y = Node<positionT, indexA, valueA>(Z.get_right(), _file, iA, vA);
			Node<positionT, indexA, valueA> X = Node<positionT, indexA, valueA>(Z.get_parent(), _file, iA, vA);
			if (X.get_left() != Z.get_pos())
				throw std::logic_error("X.get_left() != Z.get_pos()");
			if (Y.get_left() != 0)
			{
				Node<positionT, indexA, valueA> t3 = Node<positionT, indexA, valueA>(Y.get_left(), _file, iA, vA);
				Z.set_right(t3.get_pos());
				t3.set_parent(Z.get_pos());
			}
			else
			{
				Z.set_right(0);
			}
			Y.set_left(Z.get_pos());
			Z.set_parent(Y.get_pos());
			if (Y.get_right() != 0)
			{
				Node<positionT, indexA, valueA> t2 = Node<positionT, indexA, valueA>(Y.get_right(), _file, iA, vA);
				X.set_left(t2.get_pos());
				t2.set_parent(X.get_pos());
			}
			else
			{
				X.set_left(0);
			}
			Y.set_right(X.get_pos());
			if (X.get_parent() == 0)
			{
				set_root_pos(Y.get_pos());
				Y.set_parent(0);
			}
			else
			{
				Node<positionT, indexA, valueA> parents_parent = Node<positionT, indexA, valueA>(X.get_parent(), _file, iA, vA);
				if (parents_parent.get_left() == X.get_pos())
				{
					parents_parent.set_left(Y.get_pos());
				}
				else if (parents_parent.get_right() == X.get_pos())
				{
					parents_parent.set_right(Y.get_pos());
				}
				else
				{
					throw std::logic_error("X is no child of parents_parent!");
				}
				Y.set_parent(parents_parent.get_pos());
			}
			X.set_parent(Y.get_pos());
			if (Y.get_balance() == 0) {
				X.set_balance(0);
				Z.set_balance(0);
			}
			else
				if (Y.get_balance() > 0) {
					X.set_balance(0);
					Z.set_balance(-1);
				}
				else
				{
					X.set_balance(1);
					Z.set_balance(0);
				}
			Y.set_balance(0);
		}

		void rotateRightLeft(Node<positionT, indexA, valueA>& n)
		{
			std::cerr << "rotateRightLeft" << std::endl;
			Node<positionT, indexA, valueA> Z = n;
			Node<positionT, indexA, valueA> Y = Node<positionT, indexA, valueA>(Z.get_left(), _file, iA, vA);
			Node<positionT, indexA, valueA> X = Node<positionT, indexA, valueA>(Z.get_parent(), _file, iA, vA);
			if (X.get_right() != Z.get_pos())
				throw std::logic_error("X.get_right() != Z.get_pos()");
			if (Y.get_right() != 0)
			{
				Node<positionT, indexA, valueA> t3 = Node<positionT, indexA, valueA>(Y.get_right(), _file, iA, vA);
				Z.set_left(t3.get_pos());
				t3.set_parent(Z.get_pos());
			}
			else
			{
				Z.set_left(0);
			}
			Y.set_right(Z.get_pos());
			Z.set_parent(Y.get_pos());
			if (Y.get_left() != 0)
			{
				Node<positionT, indexA, valueA> t2 = Node<positionT, indexA, valueA>(Y.get_left(), _file, iA, vA);
				X.set_right(t2.get_pos());
				t2.set_parent(X.get_pos());
			}
			else
			{
				X.set_right(0);
			}
			Y.set_left(X.get_pos());
			if (X.get_parent() == 0)
			{
				set_root_pos(Y.get_pos());
				Y.set_parent(0);
			}
			else
			{
				Node<positionT, indexA, valueA> parents_parent = Node<positionT, indexA, valueA>(X.get_parent(), _file, iA, vA);
				if (parents_parent.get_left() == X.get_pos())
				{
					parents_parent.set_left(Y.get_pos());
				}
				else if (parents_parent.get_right() == X.get_pos())
				{
					parents_parent.set_right(Y.get_pos());
				}
				else
				{
					throw std::logic_error("X is no child of parents_parent!");
				}
				Y.set_parent(parents_parent.get_pos());
			}
			X.set_parent(Y.get_pos());
			if (Y.get_balance() == 0) {
				X.set_balance(0);
				Z.set_balance(0);
			}
			else
				if (Y.get_balance() > 0) {
					X.set_balance(-1);
					Z.set_balance(0);
				}
				else
				{
					X.set_balance(0);
					Z.set_balance(1);
				}
			Y.set_balance(0);
		}

		
		void rebalance(Node<positionT, indexA, valueA>& start, bool is_delete)
		{ // Assumes balances between -1 and 1.
			//std::cerr << "rebalancing" << std::endl;
			Node<positionT, indexA, valueA> current = start;
			if(is_delete && current.get_right() != 0 && current.get_balance() == 0)
			{
				current.set_balance(1);
				return;
			}
			if (current.get_parent() != 0)
			{
				Node<positionT, indexA, valueA> next(current.get_parent(), _file, iA, vA);
				bool leaveloop = false;
				Node<positionT, indexA, valueA> last = current;
				bool last_is_valid = false; // I don't need that, I guess - balance of leafs is always 0.
				while (!leaveloop)
				{
					if (next.get_right() == current.get_pos()) // next.balance++
					{
						if(is_delete)
							next.set_balance(next.get_balance() - 1);
						else
							next.set_balance(next.get_balance() + 1);
						if (current.get_balance() == -1)
						{
							if (next.get_balance() == 0)
							{
								leaveloop = true;
							}
							else if (next.get_balance() == 1 || next.get_balance() == -1)
							{
								last = current;
								current = next;
								last_is_valid = true;
								if (current.get_parent() != 0)
								{
									next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
								}
								else
								{
									leaveloop = true;
								}
							}
							else if (next.get_balance() == 2)
							{

								rotateRightLeft(current);
								leaveloop = true;
								if (current.get_parent() != 0)
								{
									next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
								}
								else
								{
									leaveloop = true;
								}
								last = current;
								if (current.get_balance() == 0 || next.get_balance() == 0)
									leaveloop = true;

							}
							else if (next.get_balance() == -2)
							{
								rotateRightLeft(current);
								leaveloop = true;
								if (current.get_parent() != 0)
								{
									next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
								}
								else
								{
									leaveloop = true;
								}
								last = current;
								if (current.get_balance() == 0 || next.get_balance() == 0)
									leaveloop = true;

							}
							else
							{
								print_tree("debug_1.dot");
								throw std::runtime_error("rebalance(): bad balance!");
							}
						}
						else if (current.get_balance() == 0)
						{
							if (next.get_balance() == 0)
							{
								leaveloop = true;
							}
							else if (next.get_balance() == 1 || next.get_balance() == -1)
							{
								last = current;
								current = next;
								last_is_valid = true;
								if (current.get_parent() != 0)
								{
									next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
								}
								else
								{
									leaveloop = true;
								}
							}
							else if (next.get_balance() == 2)
							{
								last = current;
								rotateLeft(current);
								leaveloop = true;
								if (current.get_balance() == 0)
									leaveloop = true;
								if (current.get_parent() != 0)
								{
									next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
								}
								else
								{
									leaveloop = true;
								}
							}
							else if (next.get_balance() == -2)
							{
								last = current;
								rotateRight(current);
								leaveloop = true;
								if (current.get_balance() == 0)
									leaveloop = true;
								if (current.get_parent() != 0)
								{
									next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
								}
								else
								{
									leaveloop = true;
								}
							}
							else
							{
								print_tree("debug_2.dot");
								throw std::runtime_error("rebalance(): bad balance!");
							}
						}
						else if (current.get_balance() == 1)
						{
							if (next.get_balance() == 0)
							{
								leaveloop = true;
							}
							else if (next.get_balance() == 1 || next.get_balance() == -1)
							{
								last = current;
								//next = current;
								current = next;
								last_is_valid = true;
								if (current.get_parent() != 0)
								{
									next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
								}
								else
								{
									leaveloop = true;
								}
							}
							else if (next.get_balance() == 2)
							{
								rotateLeft(current);
								leaveloop = true;
								last = current;
								if (current.get_parent() != 0)
								{
									next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
								}
								else
								{
									leaveloop = true;
								}
								if (current.get_balance() == 0 || next.get_balance() == 0)
									leaveloop = true;
							}
							else if (next.get_balance() == -2)
							{
								rotateRight(current);
								leaveloop = true;
								last = current;
								if (current.get_parent() != 0)
								{
									next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
								}
								else
								{
									leaveloop = true;
								}
								if (current.get_balance() == 0 || next.get_balance() == 0)
									leaveloop = true;
							}
							else
							{
								print_tree("debug_3.dot");
								throw std::runtime_error("rebalance(): bad balance!");
							}
						}
						else
						{
							print_tree("debug_4.dot");
							throw std::runtime_error("rebalance(): bad balance!");
						}
					}
					else // next.balance--
					{
						if (next.get_left() == current.get_pos())
						{
							if (is_delete)
								next.set_balance(next.get_balance() + 1);
							else
								next.set_balance(next.get_balance() - 1);
							if (current.get_balance() == -1)
							{
								if (next.get_balance() == -2)
								{
									last = current;
									rotateRight(current);
									leaveloop = true;
									if (current.get_parent() != 0)
									{
										next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
									}
									else
									{
										leaveloop = true;
									}
									if (current.get_balance() == 0 || next.get_balance() == 0)
										leaveloop = true;
								}
								else if (next.get_balance() == -1 || next.get_balance() == 1)
								{
									last = current;
									current = next;
									last_is_valid = true;
									if (current.get_parent() != 0)
									{
										next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
									}
									else
									{
										leaveloop = true;
									}

								}
								else if (next.get_balance() == 0)
								{
									leaveloop = true;
								}
								else if (next.get_balance() == 2)
								{
									last = current;
									rotateLeft(current);
									leaveloop = true;
									if (current.get_parent() != 0)
									{
										next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
									}
									else
									{
										leaveloop = true;
									}
									if (current.get_balance() == 0 || next.get_balance() == 0)
										leaveloop = true;
								}
								else
								{
									print_tree("debug_5.dot");
									throw std::runtime_error("rebalance(): bad balance!");
								}
							}
							else if (current.get_balance() == 0)
							{
								if (next.get_balance() == -2)
								{
									last = current;
									rotateRight(current);
									leaveloop = true;
									if (current.get_parent() != 0)
									{
										next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
									}
									else
									{
										leaveloop = true;
									}
									if (current.get_balance() == 0)
										leaveloop = true;
								}
								else if (next.get_balance() == -1 || next.get_balance() == 1)
								{
									last = current;
									current = next;
									last_is_valid = true;
									if (current.get_parent() != 0)
									{
										next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
									}
									else
									{
										leaveloop = true;
									}
								}
								else if (next.get_balance() == 0)
								{
									leaveloop = true;
								}
								else if (next.get_balance() == 2)
								{
									last = current;
									rotateLeft(current);
									leaveloop = true;
									if (current.get_parent() != 0)
									{
										next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
									}
									else
									{
										leaveloop = true;
									}
									if (current.get_balance() == 0 || next.get_balance() == 0)
										leaveloop = true;
								}
								else
								{
									print_tree("debug_6.dot");
									throw std::runtime_error("rebalance(): bad balance!");
								}
							}
							else if (current.get_balance() == 1)
							{
								if (next.get_balance() == -2)
								{
									rotateLeftRight(current);
									leaveloop = true;
									if (current.get_parent() != 0)
									{
										next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
									}
									else
									{
										leaveloop = true;
									}
									if (current.get_balance() == 0 || next.get_balance() == 0)
										leaveloop = true;
									last = current;

								}
								else if (next.get_balance() == -1 || next.get_balance() == 1)
								{
									last = current;
									current = next;
									last_is_valid = true;
									if (current.get_parent() != 0)
									{
										next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
									}
									else
									{
										leaveloop = true;
									}
								}
								else if (next.get_balance() == 0)
								{
									leaveloop = true;
								}
								else if (next.get_balance() == 2)
								{
									rotateRightLeft(current);
									leaveloop = true;
									if (current.get_parent() != 0)
									{
										next = Node<positionT, indexA, valueA>(current.get_parent(), _file, iA, vA);
									}
									else
									{
										leaveloop = true;
									}
									if (current.get_balance() == 0 || next.get_balance() == 0)
										leaveloop = true;
									last = current;

								}
								else
								{
									print_tree("debug_7.dot");
									throw std::runtime_error("rebalance(): bad balance!");
								}
							}
							else
							{
								print_tree("debug_8.dot");
								throw std::runtime_error("rebalance(): bad balance!");
							}
						}
						else
						{
							print_tree("debug_9.dot");
							positionT p1 = current.get_pos();
							positionT p2 = next.get_pos();
							throw std::logic_error("This should never happen! 'current' is neither left nor right child of 'next'!");
						}
					}
				} // if(current.get_parent() != 0)
			}
			else
			{
			}
		}


		positionT _pos_of_rootpos;
		std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> _file;
		valueA vA;
		indexA iA;
	};

}
#endif
