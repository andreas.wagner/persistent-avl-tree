#ifndef BUFFEREDFILEACCESSOR_H
#define BUFFEREDFILEACCESSOR_H

#include "AbstractFileAccesor.h"

namespace persistent_storage
{
	template<typename positionT>
	class BufferedFileAccessor : public persistent_storage::AbstractFileAccessor<positionT>
	{
	public:
		BufferedFileAccessor(std::filesystem::path filename) :
			file(),
			cache{}
		{
			file.open(filename, std::fstream::in | std::fstream::out | std::fstream::binary);
			physical_end = std::filesystem::file_size(filename);
		}

		~BufferedFileAccessor()
		{
			file.close();
		}

		void flush()
		{
			file.flush();
		}

		std::shared_ptr<persistent_storage::AbstractBuffer> getFromPos(positionT pos, std::size_t size) override
		{
			std::shared_ptr<persistent_storage::AbstractBuffer> ret;
			try
			{
				ret = cache.at(pos);
				if (ret->get_size() != size)
				{
					throw std::runtime_error("ret->getSize() != size");
				}
			}
			catch (std::out_of_range)
			{
				std::shared_ptr<persistent_storage::AbstractBuffer> buf = std::make_shared<persistent_storage::Buffer>(size);
				file.seekg(pos);
				file.read(buf->get_ptr(), size);
				if (file.bad())
					throw std::runtime_error("file bad()");
				cache[pos] = buf;
				ret = buf;
			}
			return ret;
		}

		void putToPos(std::shared_ptr<persistent_storage::AbstractBuffer>& buf, positionT pos) override
		{
			cache[pos] = buf;
		}

		void flushCache()
		{
			// TODO: try io_submit
			std::map<positionT, std::shared_ptr<persistent_storage::AbstractBuffer>> remaining; // avoid double-write of remaining buffers.
			for (auto& i : cache)
			{
				positionT pos = i.first;
				std::shared_ptr<persistent_storage::AbstractBuffer> buf = i.second;
				if (buf->is_dirty())
				{
					std::size_t to_write = buf->get_size();
					file.seekp(pos);
					file.write(buf->get_ptr(), to_write);
					if (file.bad())
					{
						throw std::runtime_error("file bad() in flushCache()");
					}
				}
				if (i.second.use_count() > 2) // avoid double-write of remaining buffers.
				{
					remaining[i.first] = i.second;
				}
			}
			cache.clear();
			cache.swap(remaining);
		}

		positionT reserveSpace(std::size_t size) override
		{
			positionT pos = physical_end;
			physical_end += size;
			//std::cerr << physical_end << ", " << size << std::endl;
			std::shared_ptr<persistent_storage::AbstractBuffer> buf = std::make_shared<persistent_storage::Buffer>(size);
			cache[pos] = buf;
			std::memset(buf->get_ptr(), 0, size);
			file.seekp(pos);
			file.write(buf->get_ptr(), size);
			if (file.bad())
			{
				throw std::runtime_error("file bad() in reserveSpace()");
			}
			return pos;
		}

		void removeSpaceReservation(positionT pos, std::size_t size)
		{
		}

	private:
		std::fstream file;
		positionT physical_end;
		std::map<positionT, std::shared_ptr<persistent_storage::AbstractBuffer>> cache;
	};
}
#endif
