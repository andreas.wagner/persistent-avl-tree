#ifndef PODVALUEACCESSOR_H
#define PODVALUEACCESSOR_H

namespace persistent_containers
{

template<typename T>
class PODValueAccessor
{
public:
	T get(BufferView elem)
	{
		return *(T*)elem.getPtr();
	}

	void set(BufferView elem, T val)
	{
		(*(T*)elem.getPtr()) = val;
	}
};

}
#endif