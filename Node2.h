#ifndef NODE_H
#define NODE_H

#include <cstddef>
#include "AbstractFileAccesor.h"
#include "AbstractValueAccessor.h"
#include "BufferView.h"

namespace persistent_containers
{

	template<typename positionT>
    class Node;

    template<typename positionT>
    class NodeFactory : public std::enable_shared_from_this<NodeFactory<positionT>>
    {
    public:
        NodeFactory(
            std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> file,
            std::shared_ptr<persistent_containers::AbstractComparableAccessor> indexAccessor,
            std::shared_ptr<persistent_containers::AbstractAccessor> valueAccessor) :
        _file(file),
        iA(indexAccessor),
        vA(valueAccessor)
        {
        }


        std::shared_ptr<Node<positionT>> createNode(positionT pos)
        {
            return std::make_shared<Node<positionT>>(pos, this->shared_from_this());
        }

        std::shared_ptr<NodeFactory<positionT>> get_shared_ptr()
        {
            return this->shared_from_this();
        }

        std::size_t getBytesToRead()
        {
            return sizeof(char)+iA->getBytesToRead()+vA->getBytesToRead()+3*sizeof(positionT);
        }

		std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> _file;
		std::shared_ptr<AbstractAccessor> vA;
		std::shared_ptr<AbstractComparableAccessor> iA;
    };

	template<typename positionT>
	class Node
	{
	public:
		Node(positionT pos, std::shared_ptr<NodeFactory<positionT>> orig);

		//static constexpr std::size_t size();

		/*static std::size_t size()
		{
			indexA iA{};
			valueA vA{};
			return sizeof(positionT) * 3 + +sizeof(char) /*balance*//* +iA.getBytesToRead() + vA.getBytesToRead();
		}*/

		inline positionT get_left();

		inline positionT get_right();

		inline positionT get_parent();

		inline char get_balance();


		inline void set_left(positionT pos);

		inline void set_right(positionT pos);

		inline void set_parent(positionT pos);

		inline void set_balance(char d);

		inline positionT get_pos();

		inline BufferView getIndex();

		inline BufferView getValue();

		inline void setIndex(BufferView idx);

		inline void setValue(BufferView val);

        inline std::shared_ptr<NodeFactory<positionT>> getOrigin();

		//Node<positionT> operator=(Node<positionT> other);
/*
		inline std::size_t getBytesToRead();

		std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> get_file();

		std::shared_ptr<AbstractAccessor> get_iA();

		std::shared_ptr<AbstractAccessor> get_vA();
*/

	private:
		std::shared_ptr<persistent_storage::AbstractBuffer> buf;
        std::shared_ptr<NodeFactory<positionT>> origin;
		positionT _pos;
	}; // class Node
}



namespace persistent_containers
{
	template<typename positionT>
	Node<positionT>::Node(positionT pos, std::shared_ptr<NodeFactory<positionT>> orig) :
		origin(orig),
		_pos(pos)
	{
		buf = origin->_file->getFromPos(pos, orig->getBytesToRead());
	}

	

	template<typename positionT>
	inline positionT Node<positionT>::get_left()
	{
		return *((positionT*)(buf->get_ptr() + sizeof(positionT)));
	}

	template<typename positionT>
	inline positionT Node<positionT>::get_right()
	{
		return *((positionT*)(buf->get_ptr() + sizeof(positionT)*2));
	}

	template<typename positionT>
	inline positionT Node<positionT>::get_parent()
	{
		return *((positionT*)buf->get_ptr());
	}

	template<typename positionT>
	inline char Node<positionT>::get_balance()
	{
		return *((char*)buf->get_ptr() + sizeof(positionT) * 3);
	}


	template<typename positionT>
	inline void Node<positionT>::set_left(positionT pos)
	{
		*((positionT*)(buf->get_ptr() + sizeof(positionT))) = pos;
		buf->mark_dirty();
	}

	template<typename positionT>
	inline void Node<positionT>::set_right(positionT pos)
	{
		*((positionT*)(buf->get_ptr() + sizeof(positionT)*2)) = pos;
		buf->mark_dirty();
	}

	template<typename positionT>
	inline void Node<positionT>::set_parent(positionT pos)
	{
		*((positionT*)buf->get_ptr()) = pos;
		buf->mark_dirty();
	}

	template<typename positionT>
	inline void Node<positionT>::set_balance(char d)
	{
		*((char*)buf->get_ptr() + sizeof(positionT) * 3) = d;
		buf->mark_dirty();
	}

	template<typename positionT>
	inline positionT Node<positionT>::get_pos()
	{
		return _pos;
	}


	template<typename positionT>
	inline BufferView Node<positionT>::getIndex()
	{
		return origin->iA->getElementFrom(buf->get_ptr() + sizeof(positionT) * 3 + sizeof(char));
	}

	template<typename positionT>
	inline BufferView Node<positionT>::getValue()
	{
		return origin->vA->getElementFrom(buf->get_ptr() + sizeof(positionT) * 3 + +sizeof(char) + origin->iA->getBytesToRead());
	}

	template<typename positionT>
	inline void Node<positionT>::setIndex(BufferView idx)
	{
		buf->mark_dirty();
		origin->iA->putElementTo(buf->get_ptr() + sizeof(positionT) * 3 + sizeof(char), idx);
	}

	template<typename positionT>
	inline void Node<positionT>::setValue(BufferView val)
	{
		buf->mark_dirty();
		origin->vA->putElementTo(buf->get_ptr() + sizeof(positionT) * 3 + +sizeof(char) + origin->iA->getBytesToRead(), val);
	}    

    template<typename positionT>
    inline std::shared_ptr<NodeFactory<positionT>> Node<positionT>::getOrigin()
    {
        return origin;
    }

	/*
	Node<typename positionT, typename depthT>
	inline Node<positionT> Node<positionT>::operator=(Node<positionT> other)
	{
		iA = other.iA;
		vA = other.vA;
		buf = other.buf;
		_file = other._file;
		_pos = other._pos;
		return *this;
	}
	*/

/*
	template<typename positionT>
	inline std::size_t Node<positionT>::getBytesToRead()
	{
		return sizeof(positionT) * 3 + sizeof(char) + orig->iA->getBytesToRead() + orig->vA->getBytesToRead(); // pointers to left, right, parent, index and value as well as balance
	}

	template<typename positionT>
	inline std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> Node<positionT>::get_file()
	{
		return _file;
	}

	template<typename positionT>
	inline std::shared_ptr<AbstractAccessor> Node<positionT>::get_iA()
	{
		return iA;
	}

	template<typename positionT>
	inline std::shared_ptr<AbstractAccessor> Node<positionT>::get_vA()
	{
		return vA;
	}
*/

}

#endif
