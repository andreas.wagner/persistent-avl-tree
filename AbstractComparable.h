#ifndef ABSTRACTCOMPARABLE_H
#define ABSTRACTCOMPARABLE_H

#include "BufferView.h"

namespace persistent_containers
{

class AbstractComparable
{
public:
	/** compare two MemoryBuffers
	@return -1 if a > b; 0 if a==b and 1 if a < b
	*/
	virtual char compare(
		BufferView a,	///< element a to compare
		BufferView b  ///< element b to compare
	)
	{
		return 0;
	}
};

}

#endif
