Work in progress

These are two persistent containers:
* An AVL-Tree containing key-value-pairs and
* a forward-list vontaining values.

The core of these two containers is datatype-independent. That reduces the bytecode produced and increases cache-efficiency.

An interface using templates will be added.
