#ifndef STOREDINSIDEACCESSOR_H
#define STOREDINSIDEACCESSOR_H

#include "AbstractAccessor.h"
#include "BufferView.h"

#include <sstream>

namespace persistent_containers
{
	/** class to access an element (index or value) stored inside a node
	*/
	template<
		typename T ///< type of element to store; has to be POD (?) or be specialized
		>
	class storedInsideAccessor : public AbstractAccessor
	{
	public:
		/** constructor
		*/
		storedInsideAccessor()
		{
		};

		/** return number of bytes to read, when accessing data within a node
		*/
		virtual std::size_t getBytesToRead() override
		{
			return sizeof(T);
		}

		/** extract memory slice holding the element
		@return BufferView of size sizeof(T) on position pos
		*/
		virtual BufferView getElementFrom(
			char* pos ///< position of element in memory; has to be within a node
			)
		{
			return BufferView(pos, sizeof(T));
		}
		
		
		/** debug-function -- use std::stringstream to convert BufferView to string
		@return std::string representing mb in type of T
		*/
		/*
		virtual std::string getString(
			BufferView mb ///< memory slice to convert
			)
		{
			std::stringstream s;
			s << *(T*)mb.getPtr();;
			return s.str();
		}
		*/

		/** put memory slice into node on position pos
		*/
		virtual void putElementTo(
			char* pos,			///< position in memory to place elem; has to be within a node
			BufferView& elem	///< memory slice to put into node 
			) override
		{
			*((T*)pos) = *(T*)elem.getPtr();
		}
		
		T get(BufferView elem)
		{
			return *(T*)elem.getPtr();
		}
		
		void set(BufferView elem, T val)
		{
			(*(T*)elem.getPtr()) = val;
		}
		

		
	};
}

#endif
