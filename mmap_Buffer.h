#ifndef MMAP_BUFFER_H
#define MMAP_BUFFER_H

#include "AbstractBuffer.h"
#include <memory>

namespace persistent_storage
{
	/** slice of mmaped space
	*/
	class mmap_Buffer : public AbstractBuffer
	{
	public:
		/** constructor
		*/
		mmap_Buffer(
			std::size_t size,	///< size of buffer 
			char* pos			///< position of buffer in memory
			);

		/** returns position in memory
		@return position in memory
		*/
		char* get_ptr() override;

		/** returns size of memory slice
		@return size of memory slice
		*/
		constexpr std::size_t get_size() override;

		/** shared pointers use count
		@return shared poiners use count
		*/
		long get_ptr_usecount() override;

	private:
		std::shared_ptr<char> _ptr;
		std::size_t _size;
	}; // class mmap_Buffer
}


namespace persistent_storage
{
	mmap_Buffer::mmap_Buffer(std::size_t size, char* pos) :
		_ptr(pos),
		_size(size)
	{
	}

	char* mmap_Buffer::get_ptr()
	{
		return _ptr.get();
	}

	constexpr std::size_t mmap_Buffer::get_size()
	{
		return _size;
	}

	long mmap_Buffer::get_ptr_usecount()
	{
		return _ptr.use_count();
	}
}
#endif
