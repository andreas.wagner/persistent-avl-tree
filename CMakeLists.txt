cmake_minimum_required(VERSION 3.16)
Project(PersistentAvlTree)



add_executable(test-transaction "test-transact.cpp" "AbstractBuffer.h"  "Buffer.h" "mmap_Buffer.h" "Node.h" "AbstractFileAccesor.h" "avl_exceptions.h" "AbstractAccessor.h" "forward-list.h" "FreespaceManagingFileAccessor.h" "BufferView.h" "AbstractValueAccessor.h" "AbstractComparable.h" "storedInsideComparableAccessor.h")
target_compile_features(test-transaction PRIVATE cxx_std_20)
