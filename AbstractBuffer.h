#ifndef ABSTRACT_BUFFER_H
#define ABSTRACT_BUFFER_H

#include <cstddef>
#include <stdexcept>

namespace persistent_storage
{
	/** interface for buffers
	*/
	class AbstractBuffer
	{
	public:
		/** constructor for invalid abstract buffer
		*/
		AbstractBuffer();

		AbstractBuffer(AbstractBuffer&) = delete;

		/** returns nullptr
		@return always nullptr;
		*/
		virtual char* get_ptr();

		/** returns 0
		@return always 0
		*/
		virtual std::size_t get_size();

		/** shared-pointers use count
		@return use count of shared pointer, if available
		*/
		virtual long get_ptr_usecount();

		/** marks buffer dirty
		*/
		void mark_dirty();

		/** true if buffer is dirty
		@return dirty flag
		*/
		bool is_dirty();

	private:
		bool dirty;
	};
}



namespace persistent_storage
{
	AbstractBuffer::AbstractBuffer() :
		dirty(false)
	{
	}

	/*
	AbstractBuffer::AbstractBuffer(AbstractBuffer&)
	{
		throw std::logic_error("copying AbstractBuffer is prohibited.");
	}
	*/

	char* AbstractBuffer::get_ptr()
	{
		return nullptr;
	}

	std::size_t AbstractBuffer::get_size()
	{
		return 0;
	}

	long AbstractBuffer::get_ptr_usecount()
	{
		return -1;
	}

	void AbstractBuffer::mark_dirty()
	{
		dirty = true;
	}

	bool AbstractBuffer::is_dirty()
	{
		return dirty;
	}
}

#endif // ABSTRACT_BUFFER_H

