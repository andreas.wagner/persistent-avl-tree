#ifndef TREE_H
#define TREE_H

#include "AbstractFileAccesor.h"
#include "AbstractBuffer.h"
#include "AbstractAccessor.h"

#include "BufferView.h"

#include "Node2.h"

#include <iterator>

namespace persistent_containers
{
	/** persistent container storing MemoryBuffers in a file
	*/
	template<
		typename positionT ///< type of the seek-pointer - can be any (unsigned) integer type
		>
	class Tree
	{
	public:
		/** constructor
		*/
		Tree(std::shared_ptr<
			persistent_storage::AbstractFileAccessor<positionT>> fileAccessor, ///< interface for accessing a file
			std::size_t pos_of_rootpos, ///< position of the root-pointer
			std::shared_ptr<AbstractComparableAccessor> indexAccessor, ///< interface for the index
			std::shared_ptr<AbstractAccessor> valueAccessor) ///< interface for the value
			:
			MyNodeFactory(std::make_shared<NodeFactory<positionT>>(fileAccessor, indexAccessor, valueAccessor)),
			_pos_of_rootpos(pos_of_rootpos)
		{
		}

		/** iterator-class
		*/
		class iterator : public std::iterator<
			std::bidirectional_iterator_tag,
			std::pair<BufferView, BufferView>,
			std::pair<BufferView, BufferView>,
			const std::pair<BufferView, BufferView>*,
			std::pair<BufferView, BufferView>>
		{
		public:
			/** constructor
			*/
			explicit iterator(
				BufferView index,	///< index to search
				bool use_closest,	///< if true, use the closest neighbour of the index in question
				Tree& t				///< tree to use
				) :
				current_node(t.find(index, use_closest))
			{
			}
			
			/** constructor
			*/
			explicit iterator(
				positionT pos,	///< position in tree
				Tree& t			///< tree to use
				) :
				current_node(t.MyNodeFactory->createNode(pos))
			{
			}
			
			/** invalid constructor for end()
			*/
			explicit iterator() :
				current_node()
			{}
			
			//explicit iterator(Tree<positionT>::iterator& i) = default;

			/** increment operator, switching to the next position in sort-order
			*/
			iterator& operator++()
			{
				if (current_node->get_right() != 0)
				{
					current_node = current_node->getOrigin()->createNode(current_node->get_right());
					while (current_node->get_left() != 0)
					{
						current_node = current_node->getOrigin()->createNode(current_node->get_left());
					}
					return *this;
				}
				else
				{
					positionT former_pos = current_node->get_pos();
					do
					{
						former_pos = current_node->get_pos();
						if (current_node->get_parent() != 0)
						{
							current_node = current_node->getOrigin()->createNode(current_node->get_parent());
							
						}
						else
						{
							current_node.reset();
							return *this;
						}
					} while (current_node->get_left() != former_pos);
				}
				return *this;
			}

			/** decrement-operator, switching to the previous position in sort-order
			*/
			iterator& operator--()
			{
				if (current_node->get_left() != 0)
				{
					current_node = current_node->getOrigin()->createNode(current_node->get_left());
					while (current_node->get_right() != 0)
					{
						current_node = current_node->getOrigin()->createNode(current_node->get_right());
					}
					return *this;
				}
				else
				{
					positionT former_pos = current_node->get_pos();
					do
					{
						former_pos = current_node->get_pos();
						if (current_node->get_parent() != 0)
						{
							current_node = current_node->getOrigin()->createNode(current_node->get_parent());
						}
						else
						{
							current_node.reset();
							return *this;
						}
					} while (current_node->get_right() != former_pos);
				}
				return *this; // TODO: is that correct behaviour?
			}

			/** equivalence-check
			*/
			bool operator==(
				iterator& other	///< the operand to compare this with
				) const
			{
				if (other.current_node && current_node)
					return current_node->get_pos() == other.current_node->get_pos();
				else
					return (!other.current_node && !current_node);
			}

//#ifdef _WIN32 //FIXME: "ambigous overload for..." on some g++, vsc++
//			bool operator!=(iterator other) const { return !(*this == other); }
//#endif

			/** reference-operator
			@return std::pair of index and value
			*/
			std::pair<BufferView, BufferView> operator* ()
			{
				return std::pair<BufferView, BufferView>(current_node->getIndex(), current_node->getValue());
			}

			/** increment operator, switching to the next position in sort-order
			*/
			iterator operator++(int) { iterator retval = *this; ++(*this); return retval; }
			
			/** decrement-operator, switching to the previous position in sort-order
			*/
			iterator operator--(int) { iterator retval = *this; --(*this); return retval; }

			/** return shared pointer of the node, this iterator references
			@return shared pointer of the node, this iterator references
			*/
			std::shared_ptr<Node<positionT>> get_node_ptr()
			{
				return current_node;
			}
		private:

			std::shared_ptr<Node<positionT>> current_node;
		};

		/** tree-iterator for the minimum in the tree
		@return tree-iterator for the minimum in the tree
		*/
		iterator begin()
		{
			if(get_root_pos() != 0)
			{
				std::shared_ptr<Node<positionT>> current = MyNodeFactory->createNode(get_root_pos());
				while (current->get_left() != 0)
					current = MyNodeFactory->createNode(current->get_left());
				return iterator(current->get_pos(), *this);
			}
			else
			{
				return end();
			}
		}
		
		/** invalid tree-iterator for behind-the-end
		@return invalid tree-iterator
		*/
		iterator end()
		{
			return iterator();
		}

		/** get iterator closest to the specified index
		
		@return iterator closest to the specified index
		*/
		iterator get_closest(
			BufferView index ///< index to search for
		)
		{
			return iterator(index, true, *this);
		}

		/** storing-function of the three
		*/
		iterator store(
			BufferView index,	///< BufferView of the index
			BufferView value,	///< BufferView of the value
			bool is_update		///< update value if existing?
			)
		{
			if (get_root_pos() == 0) // Empty tree?
			{
				std::shared_ptr<Node<positionT>> newNode = MyNodeFactory->createNode(MyNodeFactory->_file->reserveSpace(MyNodeFactory->getBytesToRead()));
				newNode->setIndex(index);
				newNode->setValue(value);
				set_root_pos(newNode->get_pos());
				return iterator(newNode->get_pos(), *this);
			}
			else
			{
				std::shared_ptr<Node<positionT>> closest = find(index, true); // fetch node closest to the desired index

				if (MyNodeFactory->iA->compare(closest->getIndex(), index) == 0)
				{
					if (is_update)
					{
						closest->setValue(value);
						return iterator(closest->get_pos(), *this);
					}
					else
						throw IndexAlreadyExists();
				}
				else
				{
					std::shared_ptr<Node<positionT>> newNode = MyNodeFactory->createNode(MyNodeFactory->_file->reserveSpace(MyNodeFactory->getBytesToRead()));
					newNode->setIndex(index);
					newNode->setValue(value);
					newNode->set_balance(0);
					newNode->set_parent(closest->get_pos());
					if (MyNodeFactory->iA->compare(closest->getIndex(), index) == 1)
					{
						if (closest->get_right() == 0)
						{
							closest->set_right(newNode->get_pos());
						}
						else
						{
							print_tree("shouldneverhappenA.dot");
							std::cerr << closest->get_pos() << std::endl;
							throw std::runtime_error("This should never happen. A");
						}
					}
					else if (MyNodeFactory->iA->compare(closest->getIndex(), index) == -1)
					{
						if (closest->get_left() == 0)
						{
							closest->set_left(newNode->get_pos());
						}
						else
						{
							throw std::runtime_error("This should never happen. B");
						}
					}
					rebalance(newNode, false, false /* is_delete is false so start_from_left is ignored */);
					return iterator(newNode->get_pos(), *this);
				}
			} // end insertion
		}

		void remove(BufferView index)
		{
			bool start_from_left;
			iterator to_remove(index, false, *this);
			if(to_remove == end())
			{
				throw IndexNotFound();
			}
			if (to_remove != end())
			{
				std::shared_ptr<Node<positionT>> to_del = to_remove.get_node_ptr();
				iterator rebalanceable = to_remove;
				rebalanceable++;
				
				positionT pos_to_rebalance;


				if (to_del->get_right() != 0 && to_del->get_left() != 0)
				{
					iterator to_append = to_remove;
					to_append++;
					std::shared_ptr<Node<positionT>> to_app = to_append.get_node_ptr();
					
					if (to_del->get_pos() == get_root_pos())
					{
						std::shared_ptr<Node<positionT>> app_parent = MyNodeFactory->createNode(to_app->get_parent());
						if(app_parent->get_pos() != to_del->get_pos())
						{
							if (app_parent->get_left() == to_app->get_pos())
							{
								app_parent->set_left(to_app->get_right());
								pos_to_rebalance = app_parent->get_pos();
								start_from_left = true;
							}
							else if (app_parent->get_right() == to_app->get_pos())
							{
								app_parent->set_right(to_app->get_right());
								pos_to_rebalance = app_parent->get_pos();
								start_from_left = false;
							}
							else
								throw std::logic_error("Here's something severely wrong!");
							if (to_app->get_right() != 0)
							{
								std::shared_ptr<Node<positionT>> app_right_child = MyNodeFactory->createNode(to_app->get_right());
								app_right_child->set_parent(app_parent->get_pos());
							}
							to_app->set_parent(0);
							to_app->set_right(to_del->get_right());
							to_app->set_left(to_del->get_left());
							to_app->set_balance(to_del->get_balance());
							std::shared_ptr<Node<positionT>> mod_l = MyNodeFactory->createNode(to_del->get_left());
							std::shared_ptr<Node<positionT>> mod_r = MyNodeFactory->createNode(to_del->get_right());
							mod_l->set_parent(to_app->get_pos());
							mod_r->set_parent(to_app->get_pos());
						}
						else
						{
							to_app->set_parent(0);
							start_from_left = false;
							to_app->set_left(to_del->get_left());
							std::shared_ptr<Node<positionT>> mod_l = MyNodeFactory->createNode(to_del->get_left());
							
							mod_l->set_parent(to_app->get_pos());
							pos_to_rebalance = to_app->get_pos();
							if(to_app->get_right() != 0)
							{
								to_app->set_balance(to_del->get_balance()-1);
							}
								to_app->set_balance(to_del->get_balance());
						}
							
						set_root_pos(to_app->get_pos());


					}
					else if (to_app->get_pos() == get_root_pos())
					{
						throw std::logic_error("A case was reacht which I wasn't certain about, whether it's reachable.");
					}
					else
					{
						std::shared_ptr<Node<positionT>> to_app = to_append.get_node_ptr();
						std::shared_ptr<Node<positionT>> app_parent = MyNodeFactory->createNode(to_app->get_parent());
						
						if (to_app->get_right() != 0 && to_del->get_right() != to_app->get_pos())
						{
							std::shared_ptr<Node<positionT>> app_right_child = MyNodeFactory->createNode(to_app->get_right());
							app_right_child->set_parent(app_parent->get_pos());
						}
						else if (to_app->get_right() != 0 && to_del->get_right() == to_app->get_pos())
						{
						}

						
						std::shared_ptr<Node<positionT>> parent1 = MyNodeFactory->createNode(to_del->get_parent());
						to_app->set_parent(to_del->get_parent());
						if (parent1->get_left() == to_del->get_pos())
						{
							parent1->set_left(to_app->get_pos());
						}
						else if (parent1->get_right() == to_del->get_pos())
						{
							parent1->set_right(to_app->get_pos());
						}
						else
						{
							throw std::runtime_error("bad tree"); // Todo: more verbose debug-info
						}

						
						if (to_del->get_right() != to_app->get_pos())
						{
							pos_to_rebalance = app_parent->get_pos();
							if (app_parent->get_left() == to_app->get_pos())
							{
								start_from_left =true;
								app_parent->set_left(to_app->get_right());
							}
							else if (app_parent->get_right() == to_app->get_pos())
							{
								start_from_left =false;
								app_parent->set_right(to_app->get_right());
							}
							else
								throw std::logic_error("Here's something severely wrong!");
							to_app->set_right(to_del->get_right());
							to_app->set_left(to_del->get_left());
							std::shared_ptr<Node<positionT>> mod_l = MyNodeFactory->createNode(to_del->get_left());
							mod_l->set_parent(to_app->get_pos());
							std::shared_ptr<Node<positionT>> mod_r = MyNodeFactory->createNode(to_del->get_right());
							mod_r->set_parent(to_app->get_pos());
							to_app->set_balance(to_del->get_balance());
						}
						else
						{
							std::shared_ptr<Node<positionT>> del_parent = MyNodeFactory->createNode(to_del->get_parent());
							to_app->set_left(to_del->get_left());
							std::shared_ptr<Node<positionT>> mod_l = MyNodeFactory->createNode(to_del->get_left());
							mod_l->set_parent(to_app->get_pos());
							
							to_app->set_balance(to_del->get_balance());
							
							if(to_app->get_left() != 0)
							{
								pos_to_rebalance = to_app->get_pos();
								start_from_left = false;
							}
							else if(to_app->get_right() != 0)
							{
								pos_to_rebalance = to_app->get_pos();
								start_from_left = true;
							}
							else
							{
								std::cerr << "nice occasion to think";
							}
						}
					}
				}
				else // to_del->get_right() == 0 || to_del->get_left() == 0
				{
					if (to_del->get_left() != 0)
					{
						pos_to_rebalance = to_del->get_parent();
						
						if (to_del->get_pos() != get_root_pos())
						{
							std::shared_ptr<Node<positionT>> parent1 = MyNodeFactory->createNode(to_del->get_parent());

							if (parent1->get_left() == to_del->get_pos())
							{
								parent1->set_left(to_del->get_left());
								std::shared_ptr<Node<positionT>> mod_l = MyNodeFactory->createNode(to_del->get_left());
								mod_l->set_parent(parent1->get_pos());
								start_from_left = true;
							}
							else if (parent1->get_right() == to_del->get_pos())
							{
								parent1->set_right(to_del->get_left());
								std::shared_ptr<Node<positionT>> mod_r = MyNodeFactory->createNode(to_del->get_left());
								mod_r->set_parent(parent1->get_pos());
								start_from_left = false;
							}
							else
							{
								throw std::runtime_error("This error should be impossible.");
							}
						}
						else
						{
							set_root_pos(to_del->get_left());
							std::shared_ptr<Node<positionT>> mod_l = MyNodeFactory->createNode(to_del->get_left());
							mod_l->set_parent(0);
						}
					}
					else if (to_del->get_right() != 0)
					{
						if (to_del->get_pos() != get_root_pos())
						{
							std::shared_ptr<Node<positionT>> parent1 = MyNodeFactory->createNode(to_del->get_parent());

							std::shared_ptr<Node<positionT>> mod_ = MyNodeFactory->createNode(to_del->get_right());
							mod_->set_parent(parent1->get_pos());
							
							if (parent1->get_left() == to_del->get_pos())
							{
								parent1->set_left(to_del->get_right());
								start_from_left =true;
							}
							else if (parent1->get_right() == to_del->get_pos())
							{
								parent1->set_right(to_del->get_right());
								start_from_left = false;
							}
							else
							{
								throw std::runtime_error("This error should be impossible. #1");
							}
							pos_to_rebalance = parent1->get_pos();
						}
						else
						{
							set_root_pos(to_del->get_right());
							pos_to_rebalance = get_root_pos();
							start_from_left = true;
						}
					}
					else
					{
						pos_to_rebalance = to_del->get_parent();
						
						if (to_del->get_pos() != get_root_pos())
						{
							std::shared_ptr<Node<positionT>> parent1 = MyNodeFactory->createNode(to_del->get_parent());
							if (parent1->get_left() == to_del->get_pos())
							{
								start_from_left = true;
								parent1->set_left(0);
							}
							else if (parent1->get_right() == to_del->get_pos())
							{
								start_from_left = false;
								parent1->set_right(0);
							}
							else
							{
								throw std::runtime_error("This error should be impossible. #2");
							}
						}
						else
						{
							set_root_pos(0);
							pos_to_rebalance = 0;
							start_from_left = false;
						}
					}
				}
				if(pos_to_rebalance != 0)
				{
					std::shared_ptr<Node<positionT>> to_rebalance = MyNodeFactory->createNode(pos_to_rebalance);
					rebalance(to_rebalance, true, start_from_left);
				}
				// TODO: remove referenced information
//					MyNodeFactory->iA->remove()
//					MyNodeFactory->vA->remove()
				MyNodeFactory->_file->removeSpaceReservation(to_del->get_pos(), MyNodeFactory->getBytesToRead());
			}
		}


		/** retrieve value-Memorybuffer for the given index
		
		may throw IndexNotFound

		@return BufferView containing the value for the given index
		
		*/
		BufferView get(BufferView index)
		{
			return find(index, false)->getValue();
		}

		/** return root-posiotion of the tree
		Reads pos-of-root-pos from file and returns it
		@return root-position
		*/
		positionT get_root_pos()
		{
			std::shared_ptr<persistent_storage::AbstractBuffer> buf = MyNodeFactory->_file->getFromPos(_pos_of_rootpos, sizeof(positionT));
			return *((positionT*)buf->get_ptr());
		}

		/** sets the root-position in a buffer, marks this buffer dirty
		*/
		void set_root_pos(
			positionT pos	///< future root-position of the tree
			)
		{
			std::shared_ptr<persistent_storage::AbstractBuffer> buf = MyNodeFactory->_file->getFromPos(_pos_of_rootpos, sizeof(pos));
			*((positionT*)buf->get_ptr()) = pos;
			buf->mark_dirty();
		}

		/** DEBUG-data-generation: creates a Graphviz-.dot-file visualizing the current tree
		*/
		void print_tree(
			std::filesystem::path filename ///< filename to put the .dot-data in
			)
		{
			std::ofstream out(filename, std::ios_base::trunc | std::ios_base::out);
			std::cerr << "creating " << filename.string() << std::endl;
			out.flush();
			out << "digraph G\n{\n\tnode[shape=record];\n";
			out << "root[label=\"root|" << get_root_pos() << "\"]; \n";
			if (get_root_pos() != 0)
			{
				std::shared_ptr<Node<positionT>> start = MyNodeFactory->createNode(get_root_pos());
				print_subtree(start, out);
			}
			out << "}\n";
			out.flush();
		}

		
	private:


		void print_subtree(std::shared_ptr<Node<positionT>> n, std::ofstream& out)
		{
			out << "p" << n->get_pos() << "[label=\"" << "p" << n->get_pos();
			out << "|" << (int)(std::size_t)n->get_balance();

			if (n->get_left() != 0)
			{
				out << "|lp" << n->get_left();
			}
			else
			{
				out << "|-";
			}
			if (n->get_right() != 0)
			{
				out << "|rp" << n->get_right();
			}
			else
			{
				out << "|-";
			}
			
			out << "\"];" << std::endl;

			if (n->get_left() != 0)
			{
				std::shared_ptr<Node<positionT>> left = MyNodeFactory->createNode(n->get_left());
				print_subtree(left, out);
				out << "p" << n->get_pos() << " -> " << "p" << n->get_left() << "[label=\"l\"];" << std::endl;
			}
			if (n->get_right() != 0)
			{
				std::shared_ptr<Node<positionT>> right = MyNodeFactory->createNode(n->get_right());
				print_subtree(right, out);
				out << "p" << n->get_pos() << " -> " << "p" << n->get_right() << "[label=\"r\"];" << std::endl;
			}
			if (n->get_parent() != 0)
			{
				std::shared_ptr<Node<positionT>> parent_node = MyNodeFactory->createNode(n->get_parent());
				out << "p" << n->get_pos() << " -> p" << n->get_parent() << "[label=\"p\"];" << std::endl;
			}
			else
			{
				out << "p" << n->get_pos() << " -> root[label=\"p\"];";
			}

		}

		std::shared_ptr<Node<positionT>> find(BufferView index, bool return_closest)
		{
			positionT root_pos = get_root_pos();
			if (root_pos == 0)
			{
				throw EmptyTree();
			}
			std::shared_ptr<Node<positionT>> n = MyNodeFactory->createNode(root_pos);

			while (MyNodeFactory->iA->compare(n->getIndex(), index) != 0)
			{
				if (MyNodeFactory->iA->compare(n->getIndex(), index) == -1)
				{
					if (n->get_left() != 0)
						n = MyNodeFactory->createNode(n->get_left());
					else
						if (return_closest)
						{
							return n;
						}
						else
							throw IndexNotFound();
				}
				else if (MyNodeFactory->iA->compare(n->getIndex(), index) == 1)
				{
					if (n->get_right() != 0)
						n = MyNodeFactory->createNode(n->get_right());
					else
						if (return_closest)
						{
							return n;
						}
						else
							throw IndexNotFound();
				}
			}
			return n;
		}

		void rotateRight(std::shared_ptr<Node<positionT>> parent, bool is_delete)
		{
			if (parent->get_left() == 0)
				throw std::runtime_error("(parent->get_right() == 0)");
			std::shared_ptr<Node<positionT>> n = MyNodeFactory->createNode(parent->get_left());
			std::shared_ptr<Node<positionT>> parents_parent;
			bool from_left = false;
			parent->set_left(n->get_right());
			if (n->get_right() != 0)
			{
				std::shared_ptr<Node<positionT>> right_tree = MyNodeFactory->createNode(n->get_right());
				right_tree->set_parent(parent->get_pos());
			}
			if (parent->get_parent() != 0)
			{
				parents_parent = MyNodeFactory->createNode(parent->get_parent());
				if (parents_parent->get_left() == parent->get_pos())
				{
					parents_parent->set_left(n->get_pos());
					from_left = true;
				}
				else
				{
					parents_parent->set_right(n->get_pos());
				}
				n->set_parent(parents_parent->get_pos());
			}
			else //(parent.get_parent() == 0)
			{ // adjust root
				set_root_pos(n->get_pos());
				n->set_parent(0);
			}
			parent->set_parent(n->get_pos());
			if(is_delete)
			{
				if (n->get_balance() == 0)
				{
					parent->set_balance(-1);
					n->set_balance(1);
				}
				else
				{
					parent->set_balance(0);
					n->set_balance(0);
				}
			}
			else
			{
				if (parent->get_balance() == 0)
				{
					parent->set_balance(1);
					n->set_balance(-1);
				}
				else
				{
					parent->set_balance(0);
					n->set_balance(0);
				}
			}
			n->set_right(parent->get_pos());
		}

		void rotateLeft(std::shared_ptr<Node<positionT>> parent, bool is_delete)
		{
			if (parent->get_right() == 0)
				throw std::runtime_error("(parent->get_right() == 0)");
			std::shared_ptr<Node<positionT>> n = MyNodeFactory->createNode(parent->get_right());
			std::shared_ptr<Node<positionT>> parents_parent;
			bool from_left = false;
			bool n_has_left_tree = false;
			parent->set_right(n->get_left());
			if (n->get_left() != 0)
			{
				n_has_left_tree = true;
				std::shared_ptr<Node<positionT>> left_tree = MyNodeFactory->createNode(n->get_left());
				left_tree->set_parent(parent->get_pos());
			}
			if (parent->get_parent() != 0)
			{
				parents_parent = MyNodeFactory->createNode(parent->get_parent());
				if (parents_parent->get_left() == parent->get_pos())
				{
					parents_parent->set_left(n->get_pos());
					from_left = true;
				}
				else
				{
					parents_parent->set_right(n->get_pos());
				}
				n->set_parent(parents_parent->get_pos());
			}
			else //(parent.get_parent() == 0)
			{ // adjust root
				set_root_pos(n->get_pos());
				n->set_parent(0);
			}
			parent->set_parent(n->get_pos());
			if(is_delete)
			{
				if (n->get_balance() == 0)
				{
					parent->set_balance(1);
					n->set_balance(-1);
				}
				else
				{
					parent->set_balance(0);
					n->set_balance(0);
				}
			}
			else
			{
				if (parent->get_balance() == 0) //
				{
					parent->set_balance(-1);
					n->set_balance(1);
				}
				else
				{
					parent->set_balance(0);
					n->set_balance(0);
				}
			}
			n->set_left(parent->get_pos());
		}

		void rotateLeftRight(std::shared_ptr<Node<positionT>> n)
		{
			std::shared_ptr<Node<positionT>> Z = n;
			std::shared_ptr<Node<positionT>> Y = MyNodeFactory->createNode(Z->get_right());
			std::shared_ptr<Node<positionT>> X = MyNodeFactory->createNode(Z->get_parent());
			if (X->get_left() != Z->get_pos())
				throw std::logic_error("X.get_left() != Z.get_pos()");
			if (Y->get_left() != 0)
			{
				std::shared_ptr<Node<positionT>> t3 = MyNodeFactory->createNode(Y->get_left());
				Z->set_right(t3->get_pos());
				t3->set_parent(Z->get_pos());
			}
			else
			{
				Z->set_right(0);
			}
			Y->set_left(Z->get_pos());
			Z->set_parent(Y->get_pos());
			if (Y->get_right() != 0)
			{
				std::shared_ptr<Node<positionT>> t2 = MyNodeFactory->createNode(Y->get_right());
				X->set_left(t2->get_pos());
				t2->set_parent(X->get_pos());
			}
			else
			{
				X->set_left(0);
			}
			Y->set_right(X->get_pos());
			if (X->get_parent() == 0)
			{
				set_root_pos(Y->get_pos());
				Y->set_parent(0);
			}
			else
			{
				std::shared_ptr<Node<positionT>> parents_parent = MyNodeFactory->createNode(X->get_parent());
				if (parents_parent->get_left() == X->get_pos())
				{
					parents_parent->set_left(Y->get_pos());
				}
				else if (parents_parent->get_right() == X->get_pos())
				{
					parents_parent->set_right(Y->get_pos());
				}
				else
				{
					throw std::logic_error("X is no child of parents_parent!");
				}
				Y->set_parent(parents_parent->get_pos());
			}
			X->set_parent(Y->get_pos());
			if (Y->get_balance() == 0) {
				X->set_balance(0);
				Z->set_balance(0);
			}
			else
				if (Y->get_balance() > 0) {
					X->set_balance(0);
					Z->set_balance(-1);
				}
				else
				{
					X->set_balance(1);
					Z->set_balance(0);
				}
			Y->set_balance(0);
		}

		void rotateRightLeft(std::shared_ptr<Node<positionT>> n)
		{
			std::shared_ptr<Node<positionT>> Z = n;
			std::shared_ptr<Node<positionT>> Y = MyNodeFactory->createNode(Z->get_left());
			std::shared_ptr<Node<positionT>> X = MyNodeFactory->createNode(Z->get_parent());
			if (X->get_right() != Z->get_pos())
				throw std::logic_error("X.get_right() != Z.get_pos()");
			if (Y->get_right() != 0)
			{
				std::shared_ptr<Node<positionT>> t3 = MyNodeFactory->createNode(Y->get_right());
				Z->set_left(t3->get_pos());
				t3->set_parent(Z->get_pos());
			}
			else
			{
				Z->set_left(0);
			}
			Y->set_right(Z->get_pos());
			Z->set_parent(Y->get_pos());
			if (Y->get_left() != 0)
			{
				std::shared_ptr<Node<positionT>> t2 = MyNodeFactory->createNode(Y->get_left());
				X->set_right(t2->get_pos());
				t2->set_parent(X->get_pos());
			}
			else
			{
				X->set_right(0);
			}
			Y->set_left(X->get_pos());
			if (X->get_parent() == 0)
			{
				set_root_pos(Y->get_pos());
				Y->set_parent(0);
			}
			else
			{
				std::shared_ptr<Node<positionT>> parents_parent = MyNodeFactory->createNode(X->get_parent());
				if (parents_parent->get_left() == X->get_pos())
				{
					parents_parent->set_left(Y->get_pos());
				}
				else if (parents_parent->get_right() == X->get_pos())
				{
					parents_parent->set_right(Y->get_pos());
				}
				else
				{
					throw std::logic_error("X is no child of parents_parent!");
				}
				Y->set_parent(parents_parent->get_pos());
			}
			X->set_parent(Y->get_pos());
			
			if (Y->get_balance() == 0) {
				X->set_balance(0);
				Z->set_balance(0);
			}
			else
				if (Y->get_balance() > 0) {
					X->set_balance(-1);
					Z->set_balance(0);
				}
				else
				{
					X->set_balance(0);
					Z->set_balance(1);
				}
			Y->set_balance(0);
			
		}

		
		void rebalance(std::shared_ptr<Node<positionT>> start, bool is_delete, bool start_from_left)
		{
			if(is_delete)
			{
				bool from_left = start_from_left;
				
				std::shared_ptr<Node<positionT>> next = start;

				while (true)
				{
					// Adjust balance
					if (from_left) // coming from left
					{
						next->set_balance(next->get_balance() + 1);
					}
					else
					{
						next->set_balance(next->get_balance() - 1);
					}
						
					if (next->get_balance() == -2)
					{
						if(next->get_left() != 0)
						{
							std::shared_ptr<Node<positionT>> prev = MyNodeFactory->createNode(next->get_left());
							if (prev->get_balance() == 1)
							{
								rotateLeftRight(prev);
								if (prev->get_parent() != 0)
									next = MyNodeFactory->createNode(prev->get_parent());
								else
									break;
							}
							else if (prev->get_balance() == 0)
							{
								rotateRight(next, true);
								if (next->get_balance() != -1)
									next = prev;
								else
									break;
							}
							else if (prev->get_balance() == -1)
							{
								rotateRight(next, true);
								
								if (prev->get_parent() != 0)
									next = prev;
								else
									break;
							}
							else
							{
								std::cerr << (int) next->get_balance() << std::endl;
								print_tree("fail_balance.dot");
								throw std::logic_error("invalid balance");
							}
						}
						else
						{
							throw std::logic_error("negative balance and no left neighbour");
						}
					}
					else if (next->get_balance() == -1)
					{
						if(!from_left)
							break;
					}
					else if (next->get_balance() == 0)
					{
					}
					else if (next->get_balance() == 1)
					{
						if (from_left)
							break;
					}
					else if (next->get_balance() == 2)
					{
						if(next->get_right() != 0)
						{
							std::shared_ptr<Node<positionT>> prev = MyNodeFactory->createNode(next->get_right());
							if (prev->get_balance() == -1)
							{
								rotateRightLeft(prev);
								
								next = MyNodeFactory->createNode(prev->get_parent());
								if(next->get_balance() == -1)
								{
									next = MyNodeFactory->createNode(next->get_parent());
								}
							}
							else if (prev->get_balance() == 0)
							{
								rotateLeft(next, true);
								break;
							}
							else if (prev->get_balance() == 1)
							{
								rotateLeft(next, true);
								if (next->get_parent() != 0)
									next = MyNodeFactory->createNode(next->get_parent());
								else
									break;
							}
							else
							{
								std::cerr << (int) next->get_balance() << std::endl;
								print_tree("fail_balance.dot");
								throw std::logic_error("invalid balance");
							}
						}
						else
						{
							throw std::logic_error("positive balance and no right neighbour");
						}
					}
					else
					{
						std::cerr << (int) next->get_balance() << std::endl;
						print_tree("fail_balance.dot");
						throw std::logic_error("invalid balance");
					}

					if (next->get_parent() != 0)
					{
						std::shared_ptr<Node<positionT>> next2 = MyNodeFactory->createNode(next->get_parent());
						if (next2->get_left() == next->get_pos()) // coming from left
						{
							from_left = true;
						}
						else if (next2->get_right() == next->get_pos()) // coming from right
						{
							from_left = false;
						}
						else
						{
							print_tree("error_1.dot");
							std::cerr << next2->get_left() << " " << next2->get_right() << " " << next->get_pos()<< " " <<std::endl;
							throw std::logic_error("next neither left nor right child of next2.");
						}
						next=next2;
					}
					else
						break;
						
				}
			}
			else // -> !is_delete
			{
				if(start->get_parent() != 0)
				{
					std::shared_ptr<Node<positionT>> next = MyNodeFactory->createNode(start->get_parent());
					
					if(next->get_left() == start->get_pos()) // coming from left
					{
						next->set_balance(next->get_balance()-1);
					}
					else if(next->get_right() == start->get_pos()) // coming from right
					{
						next->set_balance(next->get_balance()+1);
					}
					else
					{
						print_tree("error_a.dot");
			   			throw std::logic_error("next neither left nor right child of start.");
					}
					if(next->get_parent() != 0 && next->get_balance() != 0)
					{
						std::shared_ptr<Node<positionT>> next2 = MyNodeFactory->createNode(next->get_parent());
						while(true)
						{
							// Adjust balance
							if(next2->get_left() == next->get_pos()) // coming from left
							{
								next2->set_balance(next2->get_balance()-1);
							}
							else if(next2->get_right() == next->get_pos()) // coming from right
							{
								next2->set_balance(next2->get_balance()+1);
							}
							else
							{
								print_tree("error_b.dot");
					   			throw std::logic_error("next neither left nor right child of next2.");
							}
							
							if(next2->get_balance() == -2)
							{
								if(next->get_balance() == 1)
								{
									rotateLeftRight(next);
									break;
								}
								else if(next->get_balance() == 0 || next->get_balance() == -1)
								{
									rotateRight(next2, false);
									if(next->get_parent() != 0)
										next = MyNodeFactory->createNode(next->get_parent());
									break;
								}
								else
								{
									throw std::logic_error("invalid balance");								
								}
							}
							else if (next2->get_balance() == -1)
							{
								if(next->get_balance() == -1 || next->get_balance() == 1)
								{
									next = next2;
								}
								else if(next->get_balance() == 0)
								{
									break;
								}
								else
								{
									throw std::logic_error("invalid balance");								
								}
							}
							else if(next2->get_balance() == 0)
							{
								break;
							}
							else if(next2->get_balance() == 1)
							{
								if(next->get_balance() == -1 || next->get_balance() == 1)
								{
									next = next2;
								}
								else if(next->get_balance() == 0)
								{
									break;
								}
								else
								{
									throw std::logic_error("invalid balance");								
								}
							}
							else if(next2->get_balance() == 2)
							{
								if(next->get_balance() == -1)
								{
									rotateRightLeft(next);
									break;
								}
								else if(next->get_balance() == 1 || next->get_balance() == 0)
								{
									rotateLeft(next2, false);
									if(next->get_parent() != 0)
										next = MyNodeFactory->createNode(next->get_parent());
									break;
								}
								else
								{
									throw std::logic_error("invalid balance");								
								}
							}
							else
							{
								throw std::logic_error("invalid balance");
							}
							
							if(next->get_parent() != 0)
								next2 = MyNodeFactory->createNode(next->get_parent());
							else
								break;
						}
						// -> next2->get_parent() == 0
					}
				}
			}
			
		}


		positionT _pos_of_rootpos;
		public:
		std::shared_ptr<NodeFactory<positionT>> MyNodeFactory;
/*		std::shared_ptr<persistent_storage::AbstractFileAccessor<positionT>> _file;
		std::shared_ptr<AbstractAccessor> vA;
		std::shared_ptr<AbstractAccessor> iA;*/
	};

}

int test_avl(std::size_t , persistent_containers::Tree<std::size_t> );

int test_avl(std::size_t pos, persistent_containers::Tree<std::size_t> t)
{
	if(pos != 0)
	{
		std::shared_ptr<persistent_containers::Node<std::size_t>> node = t.MyNodeFactory->createNode(pos);

		int l=0, r=0;
		if(node->get_left() != 0)
		{
			l = test_avl(node->get_left(), t);
		}
		if(node->get_right() != 0)
		{
			r = test_avl(node->get_right(), t);
		}
		int diff = r-l;
		if(diff >= -1 && diff <=1 && diff == node->get_balance())
		{
			return ((l > r)?l:r)+1;
		}
		else
		{
			std::cerr << "error in pos: " << l << " " << r
			<< " " << (int) node->get_balance() << " "<< std::to_string(pos) << std::endl; 
			abort();
		}
	}
	else
	{
		return 0;
	}
}
#endif
